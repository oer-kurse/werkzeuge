=======
Ansible
=======


.. index:: Ansible

Vorbereitungen (einmalig)
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    python3.8 -m venv ansible
    cd ansible
    pip install ansible

Aufruf (erster Test)
~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    ansible localhost -a "echo 'Hallo Du Pappnase'"

SSH-Key
~~~~~~~

Für den Login per SSH einen public/privat-Key erzeugen.

.. code:: bash


    ssh-keygen -t rsa -b 4096 -o -C norbert@koppatz.com
    Speichern als: ~/.ssh/norbert-ansible

    ls ~/.ssh | grep norber
    norbert-ansible
    norbert-ansible.pub

Struktur
--------

.. image:: ./ansible-struktur.png

Modules
-------

`https://docs.ansible.com/ansible/latest/modules/modules_by_category.html <https://docs.ansible.com/ansible/latest/modules/modules_by_category.html>`_
clone eines Git-Repositories

Roles
-----

Container für diverse Aufgabengruppen. Jede Gruppe setzt sich aus
unterschiedlichen Aufgaben zusammen.

Tasks
-----

Die Aufgaben werden in main.yml durch import-Anweisungen
zusammengestellt.

Python 2
--------

Vorbedingungen
~~~~~~~~~~~~~~

.. code:: bash


    yum install -y python-devel python-setuptools python-pip
    pip install virtualenv

Inventory
---------

hosts-Datei
~~~~~~~~~~~

- Standard-Ordner: /etc/ansible/hosts

- Welche Server werden bedient

- besser mit -i explizit eine Datei angeben, ist felxibler

.. code:: bash


    [Rolle1]
    127.0.0.1

    [Rolle2]
    168.0.0.3

Konfiguration mit YAML
----------------------

- Einrückung ist wichtig

Python3 statt Python2 verwenden
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    [Rolle1]
    127.0.0.1 ansible_python_interpreter=/usr/local/bin/python3

Ausführen
~~~~~~~~~

.. code:: bash


    ansible-playbook -i ./hosts playbook.yml

Variablen
---------

Variablen verschlüsseln/entschlüsseln
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    ansible-vault encrypt all # verschlüsseln
    ansible-vault decrypt all # entschlüsseln
    ansible-vault edit all    # direkte Bearbeitung

Variablen bearbeiten (bleiben verschlüsselt)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    ansible-vault edit all

Verwenden
~~~~~~~~~

.. code:: bash


    ansible-playbook --ask-vault-pass -i ./hosts playbook

Environmentvariablen
--------------------

.. code:: bash


    export AUTHORISIZED_KEY=~/.ssh/spezieller_key

.. code:: bash


    ansible playbook --ask-vault-pass -i ./host --private-key=spezieller_key playbook

Templates
---------
