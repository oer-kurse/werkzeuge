=============
UML-Werkzeuge
=============


.. index:: Onlinewerkzeuge

.. index:: Tools; Online

Linkliste
---------

- Erklärungen: `http://plantuml.com/ <http://plantuml.com/>`_

- Testserver: `http://www.plantuml.com/plantuml/uml <http://www.plantuml.com/plantuml/uml>`_

  - Eigenen Testserver aufsetzen (Docker und andere Installationsanleitungen):

    `https://docs.gitlab.com/ee/administration/integration/plantuml.html#creating-diagrams <https://docs.gitlab.com/ee/administration/integration/plantuml.html#creating-diagrams>`_

.. table::

- Onlinetool: `http://www.umletino.com <http://www.umletino.com>`_

- DDD: `https://www.wps.de/modeler/ <https://www.wps.de/modeler/>`_
