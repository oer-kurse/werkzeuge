﻿.. index:: mercurial, hg, repo, Repo; hg

===========
 Mercurial
===========

Versionskontrolle aktivieren
============================
::

   cd <projectordner>
   # eigenes Projekt unter Versionskontrolle stellen
   hg init
   # (verstecker Ornder .hg wird angelegt)
   # Datei mit Auschlussliste, was nicht ins Repo soll
   touch .hgignore

   # oder vorhandenes Projekt beziehen
   hg clone <Pfad zum Projekt-Repository>
   
Beispiel Inhalt einer *.hgignore*
::

   syntax: glob
   # This line is a comment, and will be skipped.
   # Empty lines are skipped too.
   # Backup files left behind by the Emacs editor.
   
   *~
   
   # Lock files used by the Emacs editor.
   # Notice that the "#" character is quoted with a backslash.
   # This prevents it from being interpreted as starting a comment.
   
   .\#*
   
   # Temporary files used by the vim editor.
   .*.swp
   
   # A hidden file created by the Mac OS X Finder.
   .DS_Store
   # temporare Dateien die mit "make html" jederzeit neu generieren kann.
   
   *.pyc
   pid.txt
   _build
   nodule.log
   build

   
Arbeit mit dem Repository
=========================
::

   # Was hat sich geändert
   hg status
   # Änderungen anderer Autoren abholen
   hg pull
   # neue Dateien hinzufügen
   hg add <xxx>
   # lokale Änderungen ein-checken
   hg ci -m "Kommentar was neu ist"
   # Überflüssige Datei aus Repo entfernen
   hg remove <xxxx>
   # Datei umbenennen
   hg move <aktueller name> <neuer name>

   
   
   
