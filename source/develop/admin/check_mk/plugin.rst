================
check_mk: Plugin
================


.. index:: check_mk; plugin

Linkliste
---------

- `https://github.com/nicolasenno/check_mk_plugins/blob/master/check_ports/check_ports.py <https://github.com/nicolasenno/check_mk_plugins/blob/master/check_ports/check_ports.py>`_

- `http://blog.redturtle.it/2013/01/11/how-to-write-custom-plugin-for-nagios-and-check_mk <http://blog.redturtle.it/2013/01/11/how-to-write-custom-plugin-for-nagios-and-check_mk>`_

- `https://github.com/HeinleinSupport/check_mk_extensions/blob/83f9f5b29f160e94e0f092f193cd4668d57f28e1/uname/checks/uname <https://github.com/HeinleinSupport/check_mk_extensions/blob/83f9f5b29f160e94e0f092f193cd4668d57f28e1/uname/checks/uname>`_

- `https://bastian-kuhn.de/2011/03/check_mk-check-plugin-entwicklung-teil-1/ <https://bastian-kuhn.de/2011/03/check_mk-check-plugin-entwicklung-teil-1/>`_

- `https://bastian-kuhn.de/2011/03/check_mk-check-plugin-entwicklung-teil-2/ <https://bastian-kuhn.de/2011/03/check_mk-check-plugin-entwicklung-teil-2/>`_

Alternativen: `https://alternativeto.net/software/check_mk/ <https://alternativeto.net/software/check_mk/>`_
