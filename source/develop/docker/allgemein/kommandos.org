#+TITLE: Docker: Kommandos 
#+CATEGORY: Kurs: Docker
#+STARTUP: overview
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil

#+index: Docker Kommandos
* Docker: Kommandos 

  |-----------------------------+-------------------------------------------------------------------------------|
  | Command                     | Description                                                                   |
  |-----------------------------+-------------------------------------------------------------------------------|
  | Allgemeine Befehle          |                                                                               |
  | [[docker version]]              | Welche Version?                                                               |
  | [[docker help]]                 | Hilfe mit allen Befehlen auflisten lassen...                                  |
  | [[docker search]]               | Was ist auf hub.docker.com verfügbar?                                         |
  | docker container attach     | Attach local standard input, output, and error streams to a running container |
  | docker container commit     | Create a new image from a container’s changes                                 |
  | docker container cp         | Copy files/folders between a container and the local filesystem               |
  | [[docker container create]]     | Create a new container                                                        |
  | docker container diff       | Inspect changes to files or directories on a container’s filesystem           |
  | docker container exec       | Run a command in a running container                                          |
  | [[docker image load/save][docker container export]]     | Export a container’s filesystem as a tar archive                              |
  | docker container inspect    | Display detailed information on one or more containers                        |
  | docker container kill       | Kill one or more running containers                                           |
  | docker container logs       | Fetch the logs of a container                                                 |
  | docker container ls         | List containers                                                               |
  | docker container pause      | Pause all processes within one or more containers                             |
  | docker container port       | List port mappings or a specific mapping for the container                    |
  | docker container prune      | Remove all stopped containers                                                 |
  | docker container rename     | Rename a container                                                            |
  | docker container restart    | Restart one or more containers                                                |
  | docker container rm         | Remove one or more containers                                                 |
  | docker rmi author/image:tag | Image löschen (i, weil Image, ohne i sind Container gemeint)                  |
  | [[docker container run/start][docker container run]]        | Run a command in a new container                                              |
  | [[docker container run/start][docker container start]]      | Start one or more stopped containers                                          |
  | docker container stats      | Display a live stream of container(s) resource usage statistics               |
  | [[docker container stop]]       | Stop one or more running containers                                           |
  | [[docker container top]]        | Display the running processes of a container                                  |
  | docker container unpause    | Unpause all processes within one or more containers                           |
  | docker container update     | Update configuration of one or more containers                                |
  | docker container wait       | Block until one or more containers stop, then print their exit codes          |
  |-----------------------------+-------------------------------------------------------------------------------|

#+INDEX: docker: hub.docker.com
** Befehle für hub.docker.com

   |------------------------------------------+--------------------------------------------------------------|
   | Anweisung                                | Wirkung                                                      |
   |------------------------------------------+--------------------------------------------------------------|
 
   | commit -m "Kommentar" -a "Autorenname" \ | Neues Image auf hub.docker.com hochladen ...                 |
   | <CONTAINERID> author/image:tag           |                                                              |
   | tag IMAGEID author/image:neues-tag       | Tag nach dem Upload ändern                                   |
   |------------------------------------------+--------------------------------------------------------------|

   https://raw.githubusercontent.com/jfloff/alpine-python/master/3.7-slim/Dockerfile

#+INDEX: Docker; Netzwerke
** Docker: Netzwerke   

   - Jede Dockerinstallation besitzt eine vordefinierten
     Netzwerkkonfiguration »bridge« (virtueller switch).

   - Scope = local <-- nur lokal verfügbar

    #+BEGIN_SRC bash

    docker network ls
    docker network inspect bridge
    docker info

    #+END_SRC

*** Network check im host

    #+BEGIN_SRC bash

    ipconfig.exe <- Windows
    ip a <-- linux

    #+END_SRC


* Übung / Aufgabe                                                   :private:

4.2. Eine Anwendung, die Informationen über einen Webservice abruft,
     wird geplant und implementiert.

4.3. Ein Webservice, der Informationen (für die Bürger) zur Verfügung
     stellt, wird geplant und implementiert.

* docker container create

* docker container run/start
*** Instanz starten...

    #+BEGIN_SRC bash

    docker run -dt centos sleep infinity
    # ID erfragen
    docker ps 

    # danach mit der ID das Dockerimage betreten

    docker exec -it <ID> /bin/bash

    #+END_SRC

    Eine neue Docker-Instanz starten

    #+BEGIN_SRC bash

    docker run --name web1 -d -p 8080:80 nginx
                       ^                  ^
                       |                  |
                       |                  o--- Name des Image
		       o---------------------- Name für den Container
    #+END_SRC

* docker container stop

* docker container top
* Docker Kommandos allgemein
** docker version

   Welche Version?
   #+INDEX: Docker; Version
   #+BEGIN_SRC bash

    > docker version
    
   #+END_SRC

** docker help
   
   Hilfe
   #+INDEX: Docker; Hilfe
   #+BEGIN_SRC  bash
    
   > docker help

   #+END_SRC
    

* Docker Kommandos: Images 
** Welche Images sind lokal schon vorhanden?
   #+INDEX: Images; Liste lokal
   #+BEGIN_SRC bash

   > docker image ls 
   > docker images  # Alias für image ls
   > docker ps      # laufende Container
   > docker ps -a   # alle, also auch angehaltene

   #+END_SRC

** Welche Images gibt es?
   #+INDEX: Images; Liste auf Docker-Hub
   #+BEGIN_SRC  bash
   
   > docker search hello
   
   #+END_SRC

   - Es gibt offizielle Image von Docker Inc. verifziert
   - viele andere Anbieter (Prüfen ist wichtig!)

** Download eines Image von hub.docker.com 
   #+INDEX: Images; Download
   #+BEGIN_SRC bash
   > pull author/image:tag  
   #+END_SRC


** Alle Images löschen

   #+BEGIN_SRC bash

   # Alle Abbilder/Images löschen
   docker rmi $(docker images -q)
   
   #+END_SRC

** Volumes: erzeugen


   #+INDEX: Volumes: erzeugen
   #+BEGIN_SRC bash

   docker volume create <name>

   #+END_SRC
** Container start
   
   #+INDEX: Container; start

   #+BEGIN_SRC bash

   docker run <image>
   
   #+END_SRC

   

** Container stop
   
   #+INDEX: Container; stop
   
   #+BEGIN_SRC bash

   docker stop
   
   #+END_SRC

** Container löschen
*** einzeln   

   #+INDEX: Container; löschen (einen)

   #+BEGIN_SRC bash

   docker rm <eineContainerID> 
   
   #+END_SRC
   
*** alle  Container löschen

   #+INDEX: Container; löschen (einen)

   #+BEGIN_SRC bash

   docker rm $(docker ps -a -q)

   
   #+END_SRC


* Docker: Parameter

  https://docs.docker.com/engine/reference/run/

* Parameter erklärt


  | Parameter      | Funktion                                  | Anmerkung |
  |----------------+-------------------------------------------+-----------|
  | --detach , -d  | Run container in background and print     |           |
  | --publish , -p | Publish a container’s port(s) to the host |           |
  | --volume , -v  | Bind mount a volume                       |           |
  | -t             | Allocate a pseudo-tty                     |           |
  | -i             | Keep STDIN open even if not attached      |           |

* docker search
  Suche nach geeigneten Images...

  #+BEGIN_SRC bash
  
  # docker search <suchbegriff>
  docker search python
  
  #+END_SRC

#+INDEX: export; image
#+INDEX: export; container

* docker image load/save
  
  Nach dem Speichern, auf einem anderen Rechner auspacken und starten...

#+BEGIN_SRC ditaa :file ./save-load.png :cmdline -r
  

   +---------------------------------------------------+
   | c0A0                                              |
   | Rechner 1 (z.B. Mac)                              |
   |                                                   |
   | docker save ‒o testbottle.tar pkoppatz/hellobottle|
   |                     |                             |
   |                     |                             |
   +---------------------*-----------------------------+
                         |	                        
                         |	                        
                         +---Tranfer---------+                 
                                             |
                                             |
    +-----------------------------------+    |
    | cYEL                              |    |
    | Rechner 2 (z.B. Windows)          |    |
    |                                   |    |
    |                                   |    |
    | docker load ‒i testbottle.tar  <--*----+
    |                                   |
    +-----------------------------------+

#+END_SRC

#+RESULTS:
[[file:./save-load.png]]



  #+BEGIN_SRC bash
  docker save ‒o testbottle.tar pkoppatz/hellobottle
  docker load ‒i testbottle.tar                                        
  #+END_SRC

