====================
Docker: Installation
====================


.. index:: Docker; allgemein

Docker allgemein
----------------

Docker ist eine Virtualisierungslösung und
verwaltet Images (die Basis/Templates) und
Container (laufende Instanzen von Images).

Vorteile durch Container
~~~~~~~~~~~~~~~~~~~~~~~~

- benötigen weniger Konfiguration (wirklich?)

- Kompatibilität über Platform-Grenzen

- sind unabhängigkeit von den darunter liegenden
  Betriebssystemen und/oder der Hardware

Neue Probleme durch Container
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Kann ich mehrere Container z.B. einen für einen
  Webserver und eine Datenbank starten?

- Was passiert, wenn einer der Container den Dienst
  verweigert?

.. index:: Docker; Installation

Installation
------------

- Windows:  docs.docker.com/docker-for-windows/install/

- MacOS:    docs.docker.com/docker-for-mac/install/

- Linux:   docs.docker.com/install/linux/docker-ce/ubuntu/


.. index:: Docker; Installation: Funktionstest danach

Funktionstest
~~~~~~~~~~~~~

Download und Installation eines einfachen Docker-Images, was
rückschlüsse zulässt, ob Docker korrekt funktioniert.


.. index:: Docker; erster Funktionstest

.. code:: bash


    > docker run hello-world
