=================
Docker: Kommandos
=================


.. index:: Docker Kommandos

Docker: Kommandos
-----------------

.. table::

    +-------------------------------+-------------------------------------------------------------------------------+
    | Command                       | Description                                                                   |
    +===============================+===============================================================================+
    | Allgemeine Befehle            | \                                                                             |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker version`_             | Welche Version?                                                               |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker help`_                | Hilfe mit allen Befehlen auflisten lassen...                                  |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker search`_              | Was ist auf hub.docker.com verfügbar?                                         |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container attach       | Attach local standard input, output, and error streams to a running container |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container commit       | Create a new image from a container’s changes                                 |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container cp           | Copy files/folders between a container and the local filesystem               |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker container create`_    | Create a new container                                                        |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container diff         | Inspect changes to files or directories on a container’s filesystem           |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container exec         | Run a command in a running container                                          |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker image load/save`_     | Export a container’s filesystem as a tar archive                              |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container inspect      | Display detailed information on one or more containers                        |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container kill         | Kill one or more running containers                                           |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container logs         | Fetch the logs of a container                                                 |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container ls           | List containers                                                               |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container pause        | Pause all processes within one or more containers                             |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container port         | List port mappings or a specific mapping for the container                    |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container prune        | Remove all stopped containers                                                 |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container rename       | Rename a container                                                            |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container restart      | Restart one or more containers                                                |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container rm           | Remove one or more containers                                                 |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker rmi author/image:tag   | Image löschen (i, weil Image, ohne i sind Container gemeint)                  |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker container run/start`_ | Run a command in a new container                                              |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker container run/start`_ | Start one or more stopped containers                                          |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container stats        | Display a live stream of container(s) resource usage statistics               |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker container stop`_      | Stop one or more running containers                                           |
    +-------------------------------+-------------------------------------------------------------------------------+
    | `docker container top`_       | Display the running processes of a container                                  |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container unpause      | Unpause all processes within one or more containers                           |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container update       | Update configuration of one or more containers                                |
    +-------------------------------+-------------------------------------------------------------------------------+
    | docker container wait         | Block until one or more containers stop, then print their exit codes          |
    +-------------------------------+-------------------------------------------------------------------------------+

.. index:: docker: hub.docker.com

Befehle für hub.docker.com
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. table::

.. table::

    +-------------------------------------------+----------------------------------------------+
    | commit -m "Kommentar" -a "Autorenname" \\ | Neues Image auf hub.docker.com hochladen ... |
    +-------------------------------------------+----------------------------------------------+
    | <CONTAINERID> author/image:tag            | \                                            |
    +-------------------------------------------+----------------------------------------------+
    | tag IMAGEID author/image:neues-tag        | Tag nach dem Upload ändern                   |
    +-------------------------------------------+----------------------------------------------+

`https://raw.githubusercontent.com/jfloff/alpine-python/master/3.7-slim/Dockerfile <https://raw.githubusercontent.com/jfloff/alpine-python/master/3.7-slim/Dockerfile>`_

.. index:: Docker; Netzwerke

Docker: Netzwerke
~~~~~~~~~~~~~~~~~

- Jede Dockerinstallation besitzt eine vordefinierten
  Netzwerkkonfiguration »bridge« (virtueller switch).

- Scope = local <-- nur lokal verfügbar

  .. code:: bash


      docker network ls
      docker network inspect bridge
      docker info

Network check im host
^^^^^^^^^^^^^^^^^^^^^

.. code:: bash


    ipconfig.exe <- Windows
    ip a <-- linux

Übung / Aufgabe :private:
---------------

4.2. Eine Anwendung, die Informationen über einen Webservice abruft,
     wird geplant und implementiert.

4.3. Ein Webservice, der Informationen (für die Bürger) zur Verfügung
     stellt, wird geplant und implementiert.

docker container create
-----------------------

docker container run/start
--------------------------

Instanz starten...
^^^^^^^^^^^^^^^^^^

.. code:: bash


    docker run -dt centos sleep infinity
    # ID erfragen
    docker ps 

    # danach mit der ID das Dockerimage betreten

    docker exec -it <ID> /bin/bash

Eine neue Docker-Instanz starten

.. code:: bash


    docker run --name web1 -d -p 8080:80 nginx
    		   ^                  ^
    		   |                  |
    		   |                  o--- Name des Image
    		   o---------------------- Name für den Container

docker container stop
---------------------

docker container top
--------------------

Docker Kommandos allgemein
--------------------------

docker version
~~~~~~~~~~~~~~

Welche Version?

.. index:: Docker; Version

.. code:: bash


    > docker version

docker help
~~~~~~~~~~~

Hilfe

.. index:: Docker; Hilfe

.. code:: bash


    > docker help

Docker Kommandos: Images
------------------------

Welche Images sind lokal schon vorhanden?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. index:: Images; Liste lokal

.. code:: bash


    > docker image ls 
    > docker images  # Alias für image ls
    > docker ps      # laufende Container
    > docker ps -a   # alle, also auch angehaltene

Welche Images gibt es?
~~~~~~~~~~~~~~~~~~~~~~

.. index:: Images; Liste auf Docker-Hub

.. code:: bash


    > docker search hello

- Es gibt offizielle Image von Docker Inc. verifziert

- viele andere Anbieter (Prüfen ist wichtig!)

Download eines Image von hub.docker.com
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. index:: Images; Download

.. code:: bash

    > pull author/image:tag  

Alle Images löschen
~~~~~~~~~~~~~~~~~~~

.. code:: bash


    # Alle Abbilder/Images löschen
    docker rmi $(docker images -q)

Volumes: erzeugen
~~~~~~~~~~~~~~~~~

.. index:: Volumes: erzeugen

.. code:: bash


    docker volume create <name>

Container start
~~~~~~~~~~~~~~~

.. index:: Container; start

.. code:: bash


    docker run <image>

Container stop
~~~~~~~~~~~~~~

.. index:: Container; stop

.. code:: bash


    docker stop

Container löschen
~~~~~~~~~~~~~~~~~

einzeln
^^^^^^^

.. index:: Container; löschen (einen)

.. code:: bash


    docker rm <eineContainerID> 

alle  Container löschen
^^^^^^^^^^^^^^^^^^^^^^^

.. index:: Container; löschen (einen)

.. code:: bash


    docker rm $(docker ps -a -q)

Docker: Parameter
-----------------

`https://docs.docker.com/engine/reference/run/ <https://docs.docker.com/engine/reference/run/>`_

Parameter erklärt
-----------------

.. table::

    +----------------+-------------------------------------------+-----------+
    | Parameter      | Funktion                                  | Anmerkung |
    +================+===========================================+===========+
    | --detach , -d  | Run container in background and print     | \         |
    +----------------+-------------------------------------------+-----------+
    | --publish , -p | Publish a container’s port(s) to the host | \         |
    +----------------+-------------------------------------------+-----------+
    | --volume , -v  | Bind mount a volume                       | \         |
    +----------------+-------------------------------------------+-----------+
    | -t             | Allocate a pseudo-tty                     | \         |
    +----------------+-------------------------------------------+-----------+
    | -i             | Keep STDIN open even if not attached      | \         |
    +----------------+-------------------------------------------+-----------+

docker search
-------------

Suche nach geeigneten Images...

.. code:: bash


    # docker search <suchbegriff>
    docker search python

.. index:: export; image

.. index:: export; container

docker image load/save
----------------------

Nach dem Speichern, auf einem anderen Rechner auspacken und starten...

.. image:: ./save-load.png



.. code:: bash

    docker save ‒o testbottle.tar pkoppatz/hellobottle
    docker load ‒i testbottle.tar                                        
