.. Docker documentation master file, created by
   sphinx-quickstart on Tue May 28 19:25:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

\| :ref:`genindex` | :ref:`search` |
   
Docker-Images bauen
===================

Allgemein
---------

.. toctree::
   :maxdepth: 1
   :glob:

      
   allgemein/*
   
   container-bau/prinzipien/*

Anwendungsspezifisch
--------------------

   
.. toctree::
   :maxdepth: 1
   :glob:


   
   container-bau/python/index
   container-bau/server/bsp-nginx
   
   container-bau/dbs/sqlite/sqlite
   


   
