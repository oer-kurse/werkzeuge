#+TITLE: Podman
#+CATEGORY: UML 
#+STARTUP: overview

#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil

#+index: podman

#+BEGIN_COMMENT

https://www.redhat.com/sysadmin/basic-security-principles-containers

#+END_COMMENT
** Alias für Docker
   Weil Docker und Pdman die gleichen Befehle nutzen, kann ein Alias gesetzt werden.
   #+BEGIN_SRC bash

   alias docker=podman
   
   #+END_SRC

** Userverwaltung

   Mapping von User-ID's in und ausserhalb eines Container, mit den
   folgenden Paketen kann das realisiert werden:

   - shadow-utils unter CentOS
   - uidmap package unter Debian


** Security
*** Root-Rechte

    Aus dem Container heraus sollte folgendes nicht funktionieren:

    #+BEGIN_SRC bash

    head -n4 /etc/ssh/sshd_config
    
    #+END_SRC

    Mit sudo sollte es keine Probleme geben:

    #+BEGIN_SRC bash

    sudo head -n4 /etc/ssh/sshd_config
    
    #+END_SRC

    Innerhalb eine Pod sollte es wieder funktionieren

    #+BEGIN_SRC bash

    sudo podman run -it --rm -v /etc:/host_etc alpine head -n4 /host_etc/ssh/sshd_config
    
    #+END_SRC

** runlabel

   Entspricht dem alias unter Linux.

   #+BEGIN_SRC bash

   RUN=`podman run -dt -p 80:8001 -p 7640:3280 -v /somedir:/foobar IMAGE sh myscript.sh`
   podman container runlabel RUN <imagename>
   # Was führt das runlabel aus:
   podman container runlabel --display RUN db

   #+END_SRC

*** Regeln

    - für die Verwendung eines Images immer den vollen Pfad verwenden
      und keine Abkürzungen, die aus anderen Quellen kommen können
    - Signierte Images verwenden
    - Security-Tools verwenden z. B. SELinux oder AppAmore
    - Rootless arbeiten
    - Wenn das Image läuft, Ports, Mounts... prüfen
    - runlabel --display ... verwenden

** Remote API (varlink)

   #+BEGIN_SRC bash
   
   podman varlink --timeout=0 unix:/run/user/$(id -u)/podman/io.podman
  
   #+END_SRC

   - keine TCP-Sockets verwenden
   - wenn remote (client-to-server) Verbindungen, dann varlink bridge
     Methode benutzen


 

