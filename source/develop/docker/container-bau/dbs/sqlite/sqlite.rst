=======
SQLITE3
=======


.. index:: SQLite; docker

Docker-Image bauen
------------------

das Dockerfile (die Bauanleitung)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    FROM debian:jessie
    MAINTAINER Peter Koppatz "docker@koppatz.com"
    LABEL version="latest"

    RUN apt-get update && \
    	DEBIAN_FRONTEND=noninteractive apt-get -yq install sqlite3 && \
    	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

    RUN mkdir -p /home/db

    WORKDIR /home/db
    ADD . /home/db

Dockerimage bauen
~~~~~~~~~~~~~~~~~

.. code:: bash


    docker build -t basics/sqlite .

Container starten
~~~~~~~~~~~~~~~~~

.. code:: bash


    docker container run -dit -v /home/db:/home/db --name lite basics/sqlite

Arbeiten im Container...
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    docker exec -it lite bash
    sqlite3 test.db
