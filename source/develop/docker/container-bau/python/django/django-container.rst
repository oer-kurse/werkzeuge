======
Django
======


.. index:: Docker; Develop Django

Entwicklungsumgebung für Django
-------------------------------

Ziel ist es:

1. Django in einer Container-Umgebung starten

2. die App in einem Volume entwickeln

Django als Container starten
----------------------------

- `https://docs.docker.com/compose/django/ <https://docs.docker.com/compose/django/>`_
