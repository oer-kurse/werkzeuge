


Entwicklungsumgebung für Bottle-Projekte
----------------------------------------

Projektstruktur
~~~~~~~~~~~~~~~

Die folgenden Dateien sollten im Projektordner liegen, nennen wir ihn »bottle-simple«:

.. code:: bash

    bottle-simple> tree
    .
    ├── app.py
    ├── dockerfile
    └── requirements.txt

Alle weiteren Arbeitsschritte werden in diesem Ordner ausgeführt!

Python-Bottle-App I
~~~~~~~~~~~~~~~~~~~

Programm-Code
^^^^^^^^^^^^^

.. code:: python

    from bottle import route, run, template

    @route('/hello/<name>')
    def index(name):
        return template('<b>Hello {{name}}</b> Du Pappnase!', name=name)

    run(host='0.0.0.0', port=8080)

Konfiguration als Dockerfile
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die App wird in den Container kopiert und kann dort read-only
verwendet werden.

.. code:: bash

    FROM python:3.7-slim-stretch
    ADD requirements.txt /tmp/requirements.txt
    RUN pip install -r /tmp/requirements.txt
    ADD . /code
    WORKDIR /code
    COPY . /code
    EXPOSE 8080
    CMD ["python", "app.py"]

Image bauen
^^^^^^^^^^^

.. code:: bash


    docker build -t bottle/simple .

Image starten
^^^^^^^^^^^^^

.. code:: bash


    docker run -it -p 5000:8080 bottle/simple

Browser
^^^^^^^

.. code:: bash


    open http://localhost:5000/hello/duda

Python-Bottle-App II
~~~~~~~~~~~~~~~~~~~~

Quellcode der Applikation
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: python

    from bottle import route, run, template

    @route('/hello/<name>')
    def index(name):
        return template('<b>Hello {{name}}</b> Du Pappnase!', name=name)

    run(host='0.0.0.0', port=8080)

requirements.txt
^^^^^^^^^^^^^^^^

.. code:: python

    Bottle

Konfiguration für neues Dockerimage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: bash

    FROM python:3.7-slim-stretch
    ADD requirements.txt /tmp/requirements.txt
    RUN pip install -r /tmp/requirements.txt
    ADD . /code
    WORKDIR /code
    EXPOSE 8080
    CMD ["python", "app.py"]

Image bauen
^^^^^^^^^^^

.. code:: bash


    docker build -t bottle/volume .

Image starten
^^^^^^^^^^^^^

Diesmal den Entwicklerordner als Volume einbinden

.. code:: bash


    docker run -it -p 5000:8080 -v /zum/Entwickerordner/fuer/bottle/:/code bottle/volume

Browser
^^^^^^^

.. code:: bash


    open http://localhost:5000/hello/duda
