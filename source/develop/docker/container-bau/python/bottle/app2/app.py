from bottle import route, run, template

@route('/hello/<name>')
def index(name):
    return template('<b>Hello {{name}}</b> Du Pappnase!', name=name)

run(host='0.0.0.0', port=8080)
