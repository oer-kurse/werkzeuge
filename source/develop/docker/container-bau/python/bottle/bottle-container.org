#+CATEGORY: Docker
#+STARTUP: overview
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil

* Entwicklungsumgebung für Bottle-Projekte
** Projektstruktur

   Die folgenden Dateien sollten im Projektordner liegen, nennen wir ihn »bottle-simple«:

   #+BEGIN_SRC bash
   bottle-simple> tree
   .
   ├── app.py
   ├── dockerfile
   └── requirements.txt

   #+END_SRC

   Alle weiteren Arbeitsschritte werden in diesem Ordner ausgeführt!

** Python-Bottle-App I
*** Programm-Code

    #+include: "./app1/app.py" src python

*** Konfiguration als Dockerfile

    Die App wird in den Container kopiert und kann dort read-only
    verwendet werden.

    #+include: "./app1/dockerfile" src bash

*** Image bauen

    #+BEGIN_SRC bash

    docker build -t bottle/simple .

    #+END_SRC

*** Image starten

    #+BEGIN_SRC bash

    docker run -it -p 5000:8080 bottle/simple

    #+END_SRC

*** Browser

   #+BEGIN_SRC bash

   open http://localhost:5000/hello/duda
   
   #+END_SRC

** Python-Bottle-App II
*** Quellcode der Applikation

    #+include: "./app2/app.py" src python

*** requirements.txt

    #+include: "./app2/requirements.txt" src python

*** Konfiguration für neues Dockerimage

    #+include: "./app2/dockerfile" src bash

*** Image bauen

    #+BEGIN_SRC bash

    docker build -t bottle/volume .

    #+END_SRC

*** Image starten

    Diesmal den Entwicklerordner als Volume einbinden

    #+BEGIN_SRC bash

    docker run -it -p 5000:8080 -v /zum/Entwickerordner/fuer/bottle/:/code bottle/volume

    #+End_src

*** Browser

    #+BEGIN_SRC bash

    open http://localhost:5000/hello/duda
   
    #+END_SRC
