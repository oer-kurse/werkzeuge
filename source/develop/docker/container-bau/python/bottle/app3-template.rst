
.. contents::

.. _bottletachyons:

Bottle-App (Tachyons)
---------------------

- eine Web-Applikation, die mit Bottle als Webserver arbeitet

- für die Weiterentwicklung kann ein Dockerimage erstellt werden

Download der Anwendung
~~~~~~~~~~~~~~~~~~~~~~

.. index:: Demo: Bottle-App v1 (download)

:download:`Download: erste Version <./app3/bottle-demo-v1.zip>`

Struktur und Aufbau der Anwendung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. index:: Demo: Bottle-App (Struktur)

Nach dem Einrichten einer virtuellen Umgebung und dem Entpacken
der zip-Datei, findet man folgende Struktur vor:

.. code:: txt


    .
    ├── app.py                <-- Programm
    ├── dockerfile            <-- Bauanleitung Dockerimage
    ├── logs                  <-- Testdateien
    │   ├── access.nginx.1.log
    │   └── nginx_logfile.log
    ├── requirements.txt      <-- Installationsliste für pip und Docker
    ├── static                <-- Assets für die Bottle-App
    │   ├── css
    │   ├── images
    │   └── js
    ├── templates             <-- Templats für die GUI (Jinja2)
    │   ├── gui.html
    │   └── index.html
    └── venv                  <-- virtuelle Umgebung (nicht im zip enthalten)
        ├── bin
        ├── include
        ├── lib
        └── pyvenv.cfg

Konfiguration für neues Dockerimage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    FROM python:3.7-slim-stretch
    ADD requirements.txt /tmp/requirements.txt
    RUN pip install -r /tmp/requirements.txt
    ADD . /code
    WORKDIR /code
    EXPOSE 8080
    CMD ["python", "app.py"]

Image bauen
~~~~~~~~~~~

.. code:: bash


    docker build -t bottle/tachyons .

Start der Anwendung mit Docker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mit Docker, wenn sie das Docker-Image gebaut haben:

.. code:: bash


    docker run -it -d  -p 5000:8080 -v $pwd:/code bottle/tachyons

Start in Virtueller Umgebung (ohne Docker)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Funktioniert natürlich auch ohne Docker in der virtuellen
Umgebung (optional):

.. code:: bash


    cd projekt
    source ./activate
    python app.py
