
.. contents::



Entwicklungsumgebung für Pyramid-Projekte
-----------------------------------------

Pyramid-App I (simple)
~~~~~~~~~~~~~~~~~~~~~~

Quellcode der Applikation
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: python

    # pyramid_hello_world/app.py

    from wsgiref.simple_server import make_server
    from pyramid.config import Configurator
    from pyramid.response import Response

    def simple_world(request):
        print('Request inbound!')
        return Response('Hallo von Pyramid aus Docker heraus!')


    if __name__ == '__main__':
        config = Configurator()
        config.add_route('hello', '/')
        config.add_view(simple_world, route_name='hello')
        app = config.make_wsgi_app()
        server = make_server('0.0.0.0', 6543, app)
        server.serve_forever()

requirements.txt
^^^^^^^^^^^^^^^^

.. code:: python

    pyramid

Konfiguration für neues Dockerimage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: python

    FROM python:3.7-slim-stretch

    # We copy this file first to leverage docker cache
    COPY ./requirements.txt /app/requirements.txt

    WORKDIR /app

    RUN pip install -r requirements.txt

    COPY . /app

    ENTRYPOINT [ "python" ]

    CMD [ "app.py" ]

Image bauen
^^^^^^^^^^^

.. code:: bash


    docker build -t pyramid/simple .

Image starten
^^^^^^^^^^^^^

Diesmal den Entwicklerordner als Volume einbinden

.. code:: bash


    docker run -it -p 6543:6543 pyramid/simple

Browser
^^^^^^^

.. code:: bash


    open http://localhost:6543/

Pyramid-App II (simple/volume)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Konfiguration als Dockerfile
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Der EntwicklerOrnder wird als Volume eingebunden

.. code:: python

    FROM python:3.7-slim-stretch

    # We copy this file first to leverage docker cache
    COPY ./requirements.txt /app/requirements.txt

    WORKDIR /app

    RUN pip install -r requirements.txt

    ADD . /app

    ENTRYPOINT [ "python" ]

    CMD [ "app.py" ]

Konfiguration für neues Dockerimage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: python

    FROM python:3.7-slim-stretch

    # We copy this file first to leverage docker cache
    COPY ./requirements.txt /app/requirements.txt

    WORKDIR /app

    RUN pip install -r requirements.txt

    ADD . /app

    ENTRYPOINT [ "python" ]

    CMD [ "app.py" ]

Image bauen
^^^^^^^^^^^

.. code:: bash


    docker build -t pyramid/volume .

Image starten
^^^^^^^^^^^^^

.. code:: bash


    docker run -it -p 6543:6543 -v $pwd:/app pyramid/volume

Browser
^^^^^^^

.. code:: bash


    open http://localhost:6543/
