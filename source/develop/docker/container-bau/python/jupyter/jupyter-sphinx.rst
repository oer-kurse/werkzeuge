
.. index:: Docker; Jupyter und Sphinx

Entwicklungsumgebung für Jupyter und Sphinx
-------------------------------------------

Ziel ist es:

1. Tutorials mit Jupyter Notebooks zu entwickeln

2. vollendete Notbooks im rst-Format zu exportieren

3. die Exporte in eine Sphinx-Doku einzubinden

Requirenments
~~~~~~~~~~~~~

Inhalt der Datei »requirements.txt«: 

.. code:: python


    notebook
    sphinx
    pandoc

Dockerfile
~~~~~~~~~~

Inhalt der Datei »dockerfile«:

.. code:: python


    FROM python:3.7-slim-stretch
    ADD requirements.txt /tmp/requirements.txt
    RUN pip install -r /tmp/requirements.txt
    ADD . /code
    WORKDIR /code
    ENTRYPOINT ["jupyter", "notebook", "--allow-root","--ip=0.0.0.0", "--no-browser"]

Docker-Image bauen
~~~~~~~~~~~~~~~~~~

.. code:: bash


    docker build -t jupyter-sphinx .

Start des Docker-Images
~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    docker run -it --rm \
           -p 4444:8888 \
           -v /absoluter/pfad/zu/den/quell/dateien/:/code/ \
           -t jupyter-sphinx
