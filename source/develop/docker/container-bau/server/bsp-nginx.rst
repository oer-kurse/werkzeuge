=============
Images: Nginx
=============


.. index:: Docker-Container: nginx

Images: Nginx
-------------

Beispiel: Nginx
~~~~~~~~~~~~~~~

erster Lauf
^^^^^^^^^^^

.. code:: bash

    > docker run --name my-first-nginx \ 
      -d -p 8080:80 nginx

Ist der Container gestartet?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: bash

    docker ps 

Ausgabe erfolgt in einer langen Zeile, hier untereinander

.. table::

    +--------------+------------------------+
    | CONTAINER ID | 1be7227c8d26           |
    +--------------+------------------------+
    | IMAGE        | nginx                  |
    +--------------+------------------------+
    | COMMAND      | "nginx -g 'daemon of…" |
    +--------------+------------------------+
    | CREATED      | 21 seconds ago         |
    +--------------+------------------------+
    | STATUS       | Up 20 seconds          |
    +--------------+------------------------+
    | PORTS        | 0.0.0.0:8080->80/tcp   |
    +--------------+------------------------+
    | NAMES        | my-first-nginx         |
    +--------------+------------------------+

Beispiel: Nginx -- Nutzdaten
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Container habe keine Nutzdaten, diese werden über Volumes zur Verfügung gestellt.

Volume anlegen
^^^^^^^^^^^^^^

.. index:: Volumes

.. code:: bash

    docker run --name my-first-nginx \
    -p 8080:80  \
    -v /Volumes/ram-disk/inkubator/bsp-py/html/:/usr/share/nginx/html/
    -d nginx
