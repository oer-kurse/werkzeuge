#+TITLE: Images: Nginx
#+CATEGORY: Docker
#+STARTUP: overview
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil

#+index: Docker-Container: nginx
* Images: Nginx
** Beispiel: Nginx
*** erster Lauf
    #+BEGIN_SRC bash 
    > docker run --name my-first-nginx \ 
      -d -p 8080:80 nginx
    #+END_SRC
*** Ist der Container gestartet?
    #+BEGIN_SRC bash
    docker ps 
    #+END_SRC
    Ausgabe erfolgt in einer langen Zeile, hier untereinander

    | CONTAINER ID | 1be7227c8d26           |
    | IMAGE        | nginx                  |
    | COMMAND      | "nginx -g 'daemon of…" |
    | CREATED      | 21 seconds ago         |
    | STATUS       | Up 20 seconds          |
    | PORTS        | 0.0.0.0:8080->80/tcp   |
    | NAMES        | my-first-nginx         |

** Beispiel: Nginx -- Nutzdaten 
   Container habe keine Nutzdaten, diese werden über Volumes zur Verfügung gestellt.
*** Volume anlegen
    #+INDEX: Volumes

   #+BEGIN_SRC bash 
   docker run --name my-first-nginx \
   -p 8080:80  \
   -v /Volumes/ram-disk/inkubator/bsp-py/html/:/usr/share/nginx/html/
   -d nginx
   #+END_SRC

