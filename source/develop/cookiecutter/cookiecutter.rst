============
CookieCutter
============


.. index:: CookieCutter

CookieCutter ist
----------------

- ein Kommandozeile-Werkzeug

- erstellt einfache bis komplexe Datei- und Ordner-Strukturen

- besitzt eine niedrige Lernkurve

- erlaubt neuen Mitarbeitern den schnellen Einstieg in vorhandene
  Projekte bzw. dem Entwicker einen komfortablen Start mit einer
  vordefinierten Ordnerstruktur
