.. Git - Nachschlagen documentation master file, created by
   sphinx-quickstart on Mon May 25 10:35:37 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Git
===


Git ist eine freie Software zur verteilten Versionsverwaltung von
Dateien, die durch Linus Torvalds initiiert wurde.


.. toctree::
   :maxdepth: 2
   :caption: Inhalte:

   patch/patch
   fossil
.. toctree::
   :maxdepth: 2
   :caption: FAQ:

   faq/repo-infos
   faq/bare
   faq/stach
   faq/logging
   faq/backup
   faq/alias
   branch/branch
   win/git-win
   
   
