===============
bare-Repository
===============


.. index:: Git; bare

Commit zu einer Arbeiteskopie
-----------------------------

Das funktioniert nicht und kann nur mit einem Repository erfolgen,
das den Status »bare« besitzt.

Beispiel einer Fehlermeldung:

.. code:: bash


    remote: error: refusing to update checked out branch: refs/heads/master
    remote: error: Standardmäßig wird die Aktualisierung des aktuellen Branches in einem
    remote: Nicht-Bare-Repository zurückgewiesen, da dies den Index und das Arbeits-
    remote: verzeichnis inkonsistent zu dem machen würde, was Sie gepushed haben, und
    remote: 'git reset --hard' erforderlich wäre, damit das Arbeitsverzeichnis HEAD
    remote: entspricht.
    remote: 
    remote: Sie könnten die Konfigurationsvariable 'receive.denyCurrentBranch' im
    remote: Remote-Repository auf 'ignore' oder 'warn' setzen, um den Push in den
    remote: aktuellen Branch zu erlauben; dies wird jedoch nicht empfohlen außer
    remote: Sie stellen durch andere Wege die Aktualität des Arbeitsverzeichnisses
    remote: gegenüber dem gepushten Stand sicher.
    remote: 
    remote: Um diese Meldung zu unterdrücken und das Standardverhalten zu behalten,
    remote: setzen Sie die Konfigurationsvariable 'receive.denyCurrentBranch' auf
    remote: 'refuse'.

Ist das Reopsitory vom Typ: bare?
---------------------------------

.. code:: bash


    git rev-parse --is-bare-repository

Arbeitskope zu einem »bare«-Repository wandeln
----------------------------------------------

1. Alle Dateien löschen bis auf **.git**!

2. Alle Dateien aus .git eine Ebenen höher ablegen.

   .. code:: bash


       mv .git/* .

3. Das leere **.git** löschen.

   .. code:: bash


       rmdir .git

4. In der Datei config ändern:

   .. code:: bash

       bare = true.

Alternativ einen neuen Ordner anlegen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    mkdir mein-neues-bare-repo
    cd mein-neues-bare-repo
    cp -R /von/einer/arbeitskopie/.git/* .
    # in der Datei: config

    bare = true
