==============
Infos zum Repo
==============


.. index:: Repo; infos

.. index:: Git; Infos zum repo

Aktuellen Pfad zum Repo anzeigen
--------------------------------

.. index:: Pfad zum Repository & mehr

.. code-block:: bash


    git remote show origin

URL zum Repository ausgeben
---------------------------

.. index:: url zum Repository

.. code-block:: bash


    git remote get-url origin

Ist das Reopsitory vom Typ: bare?
---------------------------------

.. code-block:: bash


    git rev-parse --is-bare-repository

Configuration im Editor
-----------------------

.. code-block:: bash


    git config -e

Umschalten zw. zwei URL
-----------------------

.. index:: Git; origin umschalten

.. index:: Git; origin umschalten

.. index:: origin umschalten; Git

.. index:: git; umschalten origin

.. index:: umschalten origin; git

Manchmal ist es notwenig, zwischen zwei Repos mit gleichem Inhalt umzuschalten.

.. code-block:: bash


    git remote set-url origin ssh://user@repo1.de/repos/mein-repo/
    # und wider zurück... 
    git remote set-url origin /home/meine/lokalen/repos/mein-repo/
    # Prüfen, welches aktuell benutzt wird:
    git remote get-url origin
