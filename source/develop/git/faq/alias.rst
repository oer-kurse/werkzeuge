=====
Alias
=====


.. index:: Git; alias

Aus git commit wird git ci
--------------------------

Mit alias-Definitionen spart man sich Tipp-Arbeit.

.. code-block:: bash


    git config --global alias.ci commit

Aus git status wird git st
--------------------------

.. code-block:: bash


    git config --global alias.ci 'commit -m'

Einzeilige Ausgabe der Logeinträge
----------------------------------

.. code-block:: bash


    git config --global alias.ll 'log --oneline'

Letzter Commit
--------------

.. code-block:: bash


    git config --global alias.last 'log -1 HEAD --stat'

Pfad zum Remote-Repository
--------------------------

.. code-block:: bash


    git config --global alias.re 'remote -v'

Liste aller globlanen Aliase
----------------------------

.. code-block:: bahs


    git config --global alias.lg 'config --global -l'

alias in .bashrc
----------------

.. code-block:: bash


    git-pullall () { for RMT in $(git remote); do git pull -v $RMT $1; done; }    
    alias git-pullall=git-pullall

    git-pushall () { for RMT in $(git remote); do git push -v $RMT $1; done; }
    alias git-pushall=git-pushall
