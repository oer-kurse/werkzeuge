======
Switch
======


.. index:: 

Verbindung zum Repo
-------------------

Auf ein anderes Repo umstellen

.. code-block:: bash


    git remote set-url origin /home/nextcloud/projekt/repos/websiterepo

Neues Repo hinzufügen
---------------------

.. code-block:: bash


    git remote add next /home/nextcloud/projekt/repos/websiterepo


.. code-block:: bash


    git config -e
    git remote -v

Automatischer push zu allen remote Repositories
-----------------------------------------------

Ein Alias: pushall

.. code-block:: bash

    git config --global alias.pushall '!f(){ \
    for var in $(git remote show); \
      do echo "pushing to $var"; \
      git push $var; done; }; f'

    git remote add next /home/next/DIZ_Mittelstand40/repos/diz-website

nach git init
-------------

.. index:: git; Umschalten repo

.. index:: Umschalten repo; git

.. code-block:: bash


    Auf origin eines anderen Server umstellen:

    git remote set-url origin ssh://user@server.com/home/repos/kurse 
    git push --set-upstream origin master
    git add .
    git commit ...
    git push

Git ssh mit anderen portal
--------------------------

.. index:: Git; Port

.. index:: Port; Git

.. code-block:: bash


    git remote set-url origin ssh://user@server.com:6543/home/repos/kurse 
