=====
Stash
=====


.. index:: Git; Stash

Stash-Liste verwerfen
---------------------

Wenn man git add . gemacht hat und anschließend mit git status
Dateien in der Liste findet, die nicht ins Reopository sollen,
kann die Registrierung mit dem folgenden Befehl zurückgezogen
werden:

.. code-block:: bash


    git reset

Anschließend kann .gitignore erweitert und git commit wiederholt
werden.

Änderungen in den Stash verschieben
-----------------------------------

Quelle: `https://opensource.com/article/21/4/git-stash <https://opensource.com/article/21/4/git-stash>`_

.. code-block:: bash


    git stash

    # mit sinnvollem Kommentar

    git stash save "remove semi-colon from schema"

Auch nicht registrierte oder ingorierte Dateien sichern
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash


    git stash -u or git stash --include-untracked # stash untracked files
    git stash -a or git stash --all # stash untracked files and ignored files.

Was liegt im Stash?
~~~~~~~~~~~~~~~~~~~

.. code-block:: bash


    git stash list

Stash-Inhalte zurückholen
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash


    git stash pop stash@{0}

Stash löschen
~~~~~~~~~~~~~

.. code-block:: bash


    git stash clear 
    git stash drop <stash_id>
