#+TITLE: Infos zum Repo
#+CATEGORY: Git
#+STARTUP: overview

#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil

#+index: Repo; infos
#+INDEX: Git; Infos zum repo

** Aktuellen Pfad zum Repo anzeigen
   #+INDEX: Pfad zum Repository & mehr
   #+BEGIN_SRC bash

   git remote show origin
   
   #+END_SRC

** URL zum Repository ausgeben
   #+INDEX: url zum Repository

   #+BEGIN_SRC bash

   git remote get-url origin
   
   #+END_SRC

** Ist das Reopsitory vom Typ: bare?

   #+BEGIN_SRC bash
   
   git rev-parse --is-bare-repository
   
   #+END_SRC

** Configuration im Editor

   #+BEGIN_SRC bash

   git config -e
   
   #+END_SRC
** Umschalten zw. zwei URL

   #+INDEX: Git; origin umschalten
   #+INDEX: Git; origin umschalten
   #+INDEX: origin umschalten; Git
   #+INDEX: git; umschalten origin
   #+INDEX: umschalten origin; git
   
   Manchmal ist es notwenig, zwischen zwei Repos mit gleichem Inhalt umzuschalten.

   #+BEGIN_SRC bash

   git remote set-url origin ssh://user@repo1.de/repos/mein-repo/
   # und wider zurück... 
   git remote set-url origin /home/meine/lokalen/repos/mein-repo/
   # Prüfen, welches aktuell benutzt wird:
   git remote get-url origin

   #+END_SRC
