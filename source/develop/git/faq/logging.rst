=======
Logging
=======


.. index:: Git; Log

Einzeiler
---------

.. index:: Log; Einzeiler

.. code:: bash


    git log --pretty=oneline

Änderungen
----------

.. index:: Log; Einzeiler

.. code:: bash


    git log --stat
