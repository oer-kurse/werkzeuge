=======================================
backup (nicht unter Versionsverwaltung)
=======================================


.. index:: Backup; wenn nicht versioniert

Backup
------

Dateien sichern, die (noch) nicht versioniert sind.

.. code-block:: bash


    git ls-files --others --exclude-standard -z |\
    xargs -0 tar rvf ~/backup-untracked.zip
