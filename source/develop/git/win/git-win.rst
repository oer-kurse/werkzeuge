=============
GIT (Windows)
=============


.. index:: GIT

Ordner für Repo anlegen
-----------------------

.. code-block:: bash


    mkdir projekt-repo.git
    cd projekt-repo.git
    git --bare init

Auf anderen Rechnern/in anderen Ordner
--------------------------------------

.. code-block:: bash


    git clone //<rechner-freigabe/pfad/zum/projekt-repo.git

Freigaben von GIT-Repos im LAN
------------------------------

.. image:: freigabe.png

Abschließend Berechtigungen bis zum git-Ordner für jedermann freigeben.

Wichtige Git-Kommandos
----------------------

.. code-block:: bash


    git clone ...
    git pull
    git add .
    git push
    git status
    git commit -m "Mein Kommentar für die Kollegen."

Konfiguration
~~~~~~~~~~~~~

.. code-block:: bash


    git config --global --edit

Identität später ändern
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash


    git commit --amend --reset-author

Git-Repo lokal zur Verfügung stellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Server starten
^^^^^^^^^^^^^^

.. code-block:: bash


    python -m http.server

Wenn der Server läuft, kann ein Zugriff von anderem Rechner erfolgen:

.. code-block:: bash


    git clone http://xxx.xxx.xxx.xxx:8000/ sphinx-by-example
