======
Branch
======


.. index:: Branch

Branch: Liste
-------------

.. index:: git; Branch: Liste

.. code-block:: bash


    git branch -a

Branch: neu anlegen
-------------------

.. index:: git: Branch; neu anlegen

Neuen Branch anlegen und dort arbeiten...

.. code-block:: bash


    git checkout new-branch

Normale Arbeit an den Dateien...

Branch in den master einpflegen
-------------------------------

.. index:: git; Branch: in master einpflegen

.. code-block:: bash


    git checkout master
    git merge new-branch

Origin wechseln
---------------

.. index:: git; Brach: Origin wechseln

Wenn das bare-Repositorie verschoben wurde, muss auch die lokale
Kopie umgestellt werden.

.. code-block:: bash


    git remote rm origin
    git remote add origin user@server.de:neues-repository/projekt.git
    git config master.remote origin
    git config master.merge refs/heads/master
    git push --set-upstream origin master

Braches löschen
---------------

.. index:: git; Branch: löschen

`https://www.ahmedbouchefra.com/delete-local-remote-git-branches/ <https://www.ahmedbouchefra.com/delete-local-remote-git-branches/>`_
