=====
Patch
=====


.. index:: patch

Link
----

`https://www.howtogeek.com/415442/how-to-apply-a-patch-to-a-file-and-create-patches-in-linux/ <https://www.howtogeek.com/415442/how-to-apply-a-patch-to-a-file-and-create-patches-in-linux/>`_

Definition
----------

Ein Patch (engl. to patch = flicken, ausbessern) in der Bedeutung
von Reparatur bzw. Nachbesserung) ist eine Korrekturauslieferung 
für Software oder Daten aus Endanwendersicht, um Fehler zu beheben.

Ausgangsdatei (das Original)
----------------------------

.. code-block:: txt


    Beschränkungen.

      Meinem Vater hat seiner gesagt:
        Mein Vater hat noch Hasen gejagt,
        Das ist dann eingegangen.
        Ich habe noch Fische gefangen,
        Nun sind die Teiche zugesetzt.
        Du selbst, mein Sohn, fängst Vögel jetzt;
        Deinem zukünftigen Sohne
        Wird verpönet die Dohne.
        Auszulassen den Jagetrieb,
        Darf er noch fangen den Molkendieb;
        Lebt einst dein Enkel auf Erden,
        Wird das auch verboten werden.

Gespeichert als »gedicht.txt« 

Zweiter Autor ergänzt
---------------------

.. code-block:: txt


    Beschränkungen.
    ===============

      Meinem Vater hat seiner gesagt:
        Mein Vater hat noch Hasen gejagt,
        Das ist dann eingegangen.
        Ich habe noch Fische gefangen,
        Nun sind die Teiche zugesetzt.
        Du selbst, mein Sohn, fängst Vögel jetzt;
        Deinem zukünftigen Sohne
        Wird verpönet die Dohne.
        Auszulassen den Jagetrieb,
        Darf er noch fangen den Molkendieb;
        Lebt einst dein Enkel auf Erden,
        Wird das auch verboten werden.

    Anmerkungen
    ~~~~~~~~~~~

    .. glossary::

       Dohne Eine Dohne (oder auch Thone) ist eine Fangschlinge
         unterschiedlicher Bauart, die vor allem in historischer Zeit zum
         Fang von Singvögeln und Schnepfen verwendet wurde.  `siehe auch
         Wikipedia <http://de.wikipedia.org/wiki/Dohne>`_

Gespeichert als gedicht-pk.txt

Statt der geänderten Datei
--------------------------

Wie so oft werden Änderungen als Korrekturvorschläge eines
Co-Autoren in einer neuen Datei verfasst und vom Autor in das
Original eingepflegt.
Was mit Prosatexten und Officedateien noch gut funktioniert, muss
im Programmierumfeld automatisiert werden. Oft kommen auch viele
kleine Änderungswünsche in schneller Folge.

Eine Lösung ist das Patchen von Dateien.

Diff: Was hat sich geändert
---------------------------

.. code-block:: bash


    diff gedicht.txt gedicht-pk.txt


.. code-block:: text


    15a16,25
    > 
    > Anmerkungen
    > ~~~~~~~~~~~
    > 
    > .. glossary::
    > 
    >    Dohne Eine Dohne (oder auch Thone) ist eine Fangschlinge
    >      unterschiedlicher Bauart, die vor allem in historischer Zeit zum
    >      Fang von Singvögeln und Schnepfen verwendet wurde.  `siehe auch
    >      Wikipedia <http://de.wikipedia.org/wiki/Dohne>`_

Mit umschließenden Text
-----------------------

Der Schalter »-u«  gibt eine bessere Orientierung, wo genau
geändert wurde, in dem ein paar Zeile vor und hinter der Änderungen
mit angezeigt werden.

.. code-block:: bash


    diff -u gedicht.txt gedicht-pk.txt

Das sieht dann wie folgt aus:


.. code-block:: text


    --- gedicht.txt	2020-05-29 14:20:24.182447936 +0200
    +++ gedicht-pk.txt	2020-05-29 14:21:30.455102851 +0200
    @@ -13,3 +13,13 @@
         Darf er noch fangen den Molkendieb;
         Lebt einst dein Enkel auf Erden,
         Wird das auch verboten werden.
    +
    +Anmerkungen
    +~~~~~~~~~~~
    +
    +.. glossary::
    +
    +   Dohne Eine Dohne (oder auch Thone) ist eine Fangschlinge
    +     unterschiedlicher Bauart, die vor allem in historischer Zeit zum
    +     Fang von Singvögeln und Schnepfen verwendet wurde.  `siehe auch
    +     Wikipedia <http://de.wikipedia.org/wiki/Dohne>`_

Statt der ganzen Datei
----------------------

Die komplette Datei mit allen Änderungen würde den Speicherbedarf
sofort verdoppeln. Warum nicht einfach nur die Änderungen schicken?

Speichern wir das Ergebnis von diff in eine Datei, wobei der Name
und die Endung der Datei beliebig sein kann, aber die Endung patch
verrät schon, worum es sich handelt:

.. code-block:: bash


    diff -u gedicht.txt gedicht-pk.txt  > gedicht.patch

Den Patch einspielen
--------------------

Und weil der Patch mit -u erstellt wurde muss er auch mit -u
eingespielt werden.

.. code-block:: bash


    patch -u gedicht.txt -i gedicht.patch

****Kontrolle****:
Da beide Dateien nun identisch sind, sollte diff keine Ausgabe
erzeugen.

.. code-block:: bash


    diff gedicht.txt gedicht-pk.txt

Was ist, wenn etwas schief geht?
--------------------------------

Absicherung durch ein Backup, oder dem Schalter »-b« :

.. code-block:: bash


    patch -u -b gedicht.txt -i gedicht.patch

Der Autor hat nun folgende Dateien:

.. code-block:: bash


    ls -al

    -rw-r--r-- 1 p4k p4k  546 Mai 29 14:41 gedicht.patch
    -rw-r--r-- 1 p4k p4k  752 Mai 29 14:45 gedicht.txt
    -rw-r--r-- 1 p4k p4k  457 Mai 29 14:45 gedicht.txt.orig

Verzeichnisse einbeziehen
-------------------------

Das Patchen ist nicht auf einzelne Dateien beschränkt:
Will man als Co-Autor alle Änderungen in Unterverzeichnissen und
auch neue Dateien weitergeben verwendet man die Schalter »-r« und
»-N«.

.. code-block:: bash


    diff -urN alt/ neu/ > gedichte.patch

Probelauf
---------

Bei vielen Änderungen kann natürlch etwas schief gehen, weshalb ein
Probelauf nicht schaden kann:

.. code-block:: bash


    patch --dry-run -ruN -d working < gedichte.patch
