=======
Fossile
=======


.. index:: Fossil -- Versionsverwaltung

Befehle
-------

.. index:: fossil; Befehle

.. index:: Befehle; fossil

Wie bei git ...

.. code-block:: bash


    fossil info
    fossil status
    fossil changes
    fossil diff
    fossil timeline
    fossil ls
    fossil branch

Neues Projekt
-------------

.. index:: fossil; Projekt -- neu

.. index:: Projekt -- neu; fossil

.. code-block:: bash


    fossil init autonome-autos.fossil
    project-id: 63ab93f5bee46142684849c45f711a47523e437f
    server-id:  0ba2f852eaeab5603689fd3722e20300ed761254
    admin-user: p4k (initial password is "abcdefg")

Loklae Kopie
------------

.. index:: fossil; clone

.. index:: clone; fossil

.. code-block:: bash


    fossil clone /repos/auto-repo.fossil
