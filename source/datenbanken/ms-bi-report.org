#+TITLE: BI-Report- Server  (Evaluation 180 Tage)
#+CATEGORY: UML 
#+STARTUP: overview

#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil

#+index: BI-Report-Server

** BI-Report-Server

*** Hinweise in der Dokumentation

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report01.pg.webp]]

*** Installations-Start

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report02.webp]]

*** Evaluation bzw. Kauf

    #+attr_rst: :width 300px
   [[./images/ms-bi-reporter/report03.webp]]

*** Lizenz

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report04.webp]]

*** Datenbank ist schon installiert

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report05.webp]]

*** Pfadangabe

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report06.webp]]

*** Verbindung zur Datenbank herstelllen

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-builderd-bericht-auf-tabellen01.webp]]

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-builderd-bericht-auf-tabellen02.webp]]

*** Report-Server-Configurations-Manager (1 von 9)

    #+attr_rst: :width 300px    
    [[./images/ms-bi-reporter/report-builderd-download.webp]]

*** Report-Server-Configurations-Manager (2 von 9)

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-manage01.webp]]

*** Report-Server-Configurations-Manager (3 von 9)

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-manage02.webp]]

*** Report-Server-Configurations-Manager (4 von 9)

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-manage03.webp]]

*** Report-Server-Configurations-Manager (5 von 9)

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-manage04.webp]]

*** Report-Server-Configurations-Manager (6 von 9)

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-manage05.webp]]

*** Report-Server-Configurations-Manager (7 von 9)

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-manage06.webp]]

*** Report-Server-Configurations-Manager (8 von 9)

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-manage07.webp]]

*** Report-Server-Configurations-Manager (9 von 9)

    #+attr_rst: :width 300px
    [[./images/ms-bi-reporter/report-manage08.webp]]
