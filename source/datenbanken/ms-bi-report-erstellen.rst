===================
BI-Report erstellen
===================


.. index:: BI-Report-Server

BI-Report Schritt zum ersten Report
-----------------------------------

Download des Power BI Report Builder
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporterstellung/screenshot02.webp
    :width: 300px

Verbindung zur Datenbank
~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporterstellung/screenshot03.webp
    :width: 300px

.. image:: ./images/ms-bi-reporterstellung/screenshot04.webp
    :width: 300px

SQL-Server-Browser
~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporterstellung/screenshot05.webp
    :width: 300px

SQL-Server-Browser (Konfiguration)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporterstellung/screenshot06.webp
    :width: 300px

Datenbank: Eigenschaften
~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporterstellung/screenshot07.webp
    :width: 300px

Neue Standardabfrage (Felder wählen)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporterstellung/screenshot08.webp
    :width: 300px

Tabelle einfügen
~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporterstellung/screenshot09.webp
    :width: 300px

.. image:: ./images/ms-bi-reporterstellung/screenshot10.webp
    :width: 300px

.. image:: ./images/ms-bi-reporterstellung/screenshot11.webp
    :width: 300px

Ergebnis
~~~~~~~~

Trick: sollte nichts angezeigt werden, hilft das Justieren der der
Spaltenhöhe. Einmal extrem viel Zeilenabstand, danach wieder auf
normal zurück, zeigte die Inhalte korrekt an. 

.. image:: ./images/ms-bi-reporterstellung/screenshot12.webp
    :width: 300px
