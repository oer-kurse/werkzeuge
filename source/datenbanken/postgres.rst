========
Postgres
========


.. index:: Installation; Postgres/Debian

.. index:: Postgres; Installation

Installation
------------

.. code-block:: bash


    apt install postgresql postgresql-client

Verbinden als root
------------------

.. code-block:: bash


    su -s /bin/bash postgres

Verbinden als sudo
------------------

.. code-block:: bash


    sudo -u postgres bash

Weiter Informationen
--------------------

- `https://wiki.debian.org/PostgreSql <https://wiki.debian.org/PostgreSql>`_
