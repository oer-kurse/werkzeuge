========================================
BI-Report- Server  (Evaluation 180 Tage)
========================================


.. index:: BI-Report-Server

BI-Report-Server
----------------

Hinweise in der Dokumentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report01.pg.webp
    :width: 300px

Installations-Start
~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report02.webp
    :width: 300px

Evaluation bzw. Kauf
~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report03.webp
    :width: 300px

Lizenz
~~~~~~

.. image:: ./images/ms-bi-reporter/report04.webp
    :width: 300px

Datenbank ist schon installiert
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report05.webp
    :width: 300px

Pfadangabe
~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report06.webp
    :width: 300px

Verbindung zur Datenbank herstelllen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-builderd-bericht-auf-tabellen01.webp
    :width: 300px

.. image:: ./images/ms-bi-reporter/report-builderd-bericht-auf-tabellen02.webp
    :width: 300px

Report-Server-Configurations-Manager (1 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-builderd-download.webp
    :width: 300px

Report-Server-Configurations-Manager (2 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-manage01.webp
    :width: 300px

Report-Server-Configurations-Manager (3 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-manage02.webp
    :width: 300px

Report-Server-Configurations-Manager (4 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-manage03.webp
    :width: 300px

Report-Server-Configurations-Manager (5 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-manage04.webp
    :width: 300px

Report-Server-Configurations-Manager (6 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-manage05.webp
    :width: 300px

Report-Server-Configurations-Manager (7 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-manage06.webp
    :width: 300px

Report-Server-Configurations-Manager (8 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-manage07.webp
    :width: 300px

Report-Server-Configurations-Manager (9 von 9)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/ms-bi-reporter/report-manage08.webp
    :width: 300px
