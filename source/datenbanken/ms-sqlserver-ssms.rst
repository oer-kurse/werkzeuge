===========================================
MS-SQL-Server -- SSMS (Evaluation 180 Tage)
===========================================


.. index:: SSMS -- SQL Server Management Studio

SQL Server Management Studio (SSMS)
-----------------------------------

Evaluation
~~~~~~~~~~

.. image:: ./images/msserver-ssms/evaluation.webp
    :width: 300px

Auch wenn man es nicht haben will...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Azure gibts dazu ...

.. image:: ./images/msserver-ssms/management-studio01.webp
    :width: 300px

Es gibt noch mehr...
~~~~~~~~~~~~~~~~~~~~

Nach der Installation finden sich eine Reihe von Tools im Windows-Menü.

.. image:: ./images/msserver-ssms/programmueberischt.webp
    :width: 300px
