================================
 Werkzeuge für die Modellierung
================================
.. |x| image:: ./werbung/katktusbluete08.jpg

.. |y| raw:: html

       <div class='hover_img'>
         <a href='#'>Kaktus
         <span>
           <img src='../../_images/katktusbluete08.jpg'
                alt='Kaktusbluete' />
          </span>
         </a>
        </div>

.. sidebar:: SQL-Kurs

	     | Statt der Werbung...
	     | |y|
	     | Serie: Pflanzen

Es muss nicht gleich ein Programm sein, meistens reichen Papier und
Stifte für den ersten Schritt.

- `Liste von Werkzeugen für die Modellierung (Wikipedia)`_
- `Liste der Werkzeuge für PostgreSQL (Wiki)`_
- `Workbench MySQL`_
- `DatabaseSpy -- Altova`_
- `Datenbank Designer -- Microsoft`_
- `SQL Developer -- Oracle`_


.. Linkliste
  
.. _Liste von Werkzeugen für die Modellierung (Wikipedia): https://de.wikipedia.org/wiki/Liste_von_Datenmodellierungswerkzeugen
.. _Liste der Werkzeuge für PostgreSQL (Wiki): https://wiki.postgresql.org/wiki/Community_Guide_to_PostgreSQL_GUI_Tools
.. _Workbench MySQL: https://dev.mysql.com/downloads/file/?id=468289
.. _DatabaseSpy -- Altova: https://www.altova.com/databasespy
.. _Datenbank Designer -- Microsoft: https://msdn.microsoft.com/de-de/library/aa292883(v=vs.71).aspx
.. _SQL Developer -- Oracle: http://www.oracle.com/technetwork/developer-tools/sql-developer/downloads/index.html

