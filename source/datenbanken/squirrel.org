#+TITLE: Squirrel
#+CATEGORY: Datenbanken
#+STARTUP: overview
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil

#+INDEX: GUI; Squirrel
#+INDEX: Tools; Squirrel
#+INDEX: Squirrel

** GUI: Squirrel

  Ein Java-Tool mit dem viele Datenbanken über eine grafische
  Benutzeroberfläche verwaltet werden können. Alle wichtigen 
  Links sind unter Installation/Vorarbeiten gelistet.

** Konfiguration I: Treiber einbinden
** Starte das Programm:  SQuirrel SQL Client:

    #+BEGIN_SRC bash

    java -jar squirrel-sql-<version>-install.jar 

    #+END_SRC

**  Driver list 

   - »Create a New Driver«
   - Einen beschreibenden Namen erfinden »Meine Verbindung«
   - »org.postgresql.Driver« im Feld »Class Name«
   - »Extra Class Path« auswählen.
     - »Add«  JDBC jar-Datei für postgresql
       z.B: postgresql-42.2.5.jar
   - alle Einstellungen bestätigen

   Hier noch ein Bild:

   [[./squirrel-treiber.png]]

** Konfiguration II: Alias definieren

   [[./squirrel-alias-definieren.png]]

** Konfiguration III: Verbinden

   [[./squirrel-alias.png]]

** Vorarbeiten: Installation

   Java installieren

   https://www.java.com/de/download/win10.jsp
   
   Download SQuirrel SQL Client
   
   http://squirrel-sql.sourceforge.net/#installation
   
   Download Postgres-Treiber
   
   https://jdbc.postgresql.org/download.html
