======
SQLite
======


.. index:: SQLite

Download & Entpacken
--------------------

`https://www.sqlite.org/download.html <https://www.sqlite.org/download.html>`_

Werkzeugen
----------

Siehe auch 

GUI: `http://www.sqliteexpert.com <http://www.sqliteexpert.com>`_


Auf SQLite basierend gibt es Erweiterungen, die das Datenbanksystem
nicht nur für die Entwicklung, sondern auch für Produktivsystene
interssant machen.

**Litestream -- Replication**
-----------------------------

.. index:: Litestream; Replication

.. index:: Replication; Litestream

*URL*: `Litestream -- Replication <https://litestream.io/>`_
*letzter Zugriff*: 2021-02-13

**Anmerkung** \:\: Litestream

Damit lassen sich Datenbanken in die Amzon-Cloud oder im
Dateisystem replizieren und so Sicherungskopien anlegen.

**Fossil -- CMS/Git**
---------------------

.. index:: Fossil; CMS/Git

.. index:: CMS/Git; Fossil

*URL*: `Fossil -- CMS/gitclone <https://fossil-scm.org/home/doc/trunk/www/index.wiki>`_
*letzter Zugriff*: 2021-02-13

**Anmerkung** \:\: Fossil

- `Git verglichen mit Fossil <https://fossil-scm.org/home/doc/trunk/www/index.wiki>`_
