=======
DBeaver
=======


.. index:: GUI; DBeaver

.. index:: Tools; DBeaver

.. index:: DBeaver

GUI: DBeaver
------------

.. image:: images/dbeaver-logo.png

Ein Java-Tool mit dem viele Datenbanken über eine grafische
Benutzeroberfläche verwaltet werden können. Alle wichtigen 
Links sind unter Installation/Vorarbeiten gelistet.

Konfiguration I: Treiber einbinden
----------------------------------

.. image:: ./images/dbeaver-treiber-postgres.png

Driver list
-----------

.. image:: ./images/dbeaver-neue-verbindung.png


.. image:: ./images/beaver-connection.png

Proxy-Einstellungen
-------------------

Wenn ein Proxy den Zugriff zum »www« steuert, über das Menü »Fenster | Einstellungen« :

Für das Laden von Treibern:

.. image:: ./images/beaver-treiber-proxy.png

Für den Zugang zum Netz

.. image:: ./images/beaver-netwerk-proxy.png
