========================================
MS-SQL-Server (Evalualtion für 180 Tage)
========================================


.. index:: 

Installation
------------

Vorab
~~~~~

- Ohne Login geht nix!

- Nach der Anmeldung, erhält man eine Meldung mit dem ersten Links:

`https://www.microsoft.com/en-us/sql-server/sql-server-downloads <https://www.microsoft.com/en-us/sql-server/sql-server-downloads>`_

Start
~~~~~

.. image:: ./images/msserver/01-neuinstallation.webp
    :width: 300px

Softwaraktualisierung
~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/01-update.webp
    :width: 300px

180 Tage testen bzw. kaufen
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/02-installation-evaluation.webp
    :width: 300px

Installations-Center
~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/02-installationscenter.webp
    :width: 300px

Key auf der DVD (bzw. der Website)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/02-schluessel.webp
    :width: 300px

Linzen-Bedingungen (nix ist erlaubt)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/03-lizen.webp
    :width: 300px

Haften tun wir aber für nix!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/04-lizenz-und-haftung.webp
    :width: 300px

Plattmachen (neu) oder erweitern
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/04-neuinstallation.webp
    :width: 300px

Update
~~~~~~

.. image:: ./images/msserver/05-update.webp
    :width: 300px

Installationsregeln
~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/06-installationsregeln.webp
    :width: 300px

Auswahl (Funktionalität)
~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/07-auswahl1.webp
    :width: 300px

Instanzkonfiguration
~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/08-instanz.webp
    :width: 300px

Serverkonfiguration
~~~~~~~~~~~~~~~~~~~

.. image:: ./images/msserver/09-serverkonfiguration.webp
    :width: 300px

Datenbank-Engine
~~~~~~~~~~~~~~~~

Hier wäre die gemischen Authentifizierung günstiger gewesen, um auch 
mit anderen Programmen auf die Datenbanken zugreifen zu könnnen.

.. image:: ./images/msserver/10-benutzer-admin.webp
    :width: 300px

Optional: Pandas
~~~~~~~~~~~~~~~~

Für Python-Fans eine Pflichtauswahl.

.. image:: ./images/msserver/11-python-anaconda.webp
    :width: 300px

Zusammenfassung
~~~~~~~~~~~~~~~

.. image:: ./images/msserver/12-zusammenfassung.webp
    :width: 300px
