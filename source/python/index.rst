
| | :ref:`genindex` | :ref:`search` |


Entwickler
==========

.. toctree::
   :glob:
   :maxdepth: 3


   install/install-win
   venv/index
   sphinx-by-example/index
   flake8/flake8
   tools


Datenanalyse
============

.. toctree::
   :maxdepth: 1
   :glob:
      
   jupyter/numpy/numpy-arrays
   jupyter/pandas/create-dataframe-from-csv
   jupyter/pandas/dataframe-manipulate
   jupyter/pandas/dataframe-select
   jupyter/pandas/dataframe-stringmanipulationen
