=================================
Python (Installation auf Windows)
=================================


.. index:: Python; Installation (Windows)

Nach dem Download
-----------------

Das Setup-Programm starten, aber einige wichtige Änderungen an den 
vorgeschlagenen Standardwerten vornehmen (eine Empfehlung).

Den Suchpfad
~~~~~~~~~~~~

unbedingt setzen lassen, um später beim Aufruf des Python-Interpreter
nicht den kompletten Pfad voranstellen zu müssen.

.. image:: ./images/python3.9-install.png

Installationordner
~~~~~~~~~~~~~~~~~~

Falls doch einmal der komplette Pfad benötigt wird, ist es angenehm,
wenn der recht kurz ist, also den Vorschlag ändern, dafür ist eine 
Angepasste (Customized) Installation notwendig


.. image:: ./images/python3.9-folder.png

Probeinstallation: Sphinx
~~~~~~~~~~~~~~~~~~~~~~~~~

Ein Projekt ohne Dokumentation muss als unvollständig und fehlerhaft 
eingestuft werden, deshalb wird eine Testinstallation für »Sphinx«  
gezeigt:


.. image:: ./images/python3.9-install-sphinx.png

Was wurde bisher Installiert?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Inklusive aller Abhängigkeiten...

.. image:: ./images/python3.9-install-freeze.png
