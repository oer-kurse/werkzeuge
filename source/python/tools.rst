===============================
Werkzeuge -- Python (Übersicht)
===============================


.. index:: Tools

.. index:: Werkzeuge, venv, env

Python-Tools -- eine Auswahl
----------------------------

Nachfolgend einige Empfehlungen, weitere Listen sind online verfügbar z.B. unter:

- `https://www.dunebook.com/best-python-ide-windows-mac/ <https://www.dunebook.com/best-python-ide-windows-mac/>`_

- `https://www.programiz.com/python-programming/ide <https://www.programiz.com/python-programming/ide>`_

Modulen installieren mit PIP
----------------------------

- pip (ist Standard)

- der Module-Index ist zu finden auf `https://pypi.org <https://pypi.org>`_

- für jedes Modul ist der Installations-Aufruf dokumentiert

Editoren
--------

WingIDE
~~~~~~~

`https://wingware.com/downloads <https://wingware.com/downloads>`_

PyCharm
~~~~~~~

.. index:: PyCharm; Einstellungen

Dateien in Karteireitern laden
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Diese Funktion deaktivieren, weil es einfachere
Umschaltmöglichkeiten gibt und immer nur eine Datei bearbeitet
wird.

.. index:: PyCharm; Tastenkombintionen

PyCharm Tastenkombinationen
^^^^^^^^^^^^^^^^^^^^^^^^^^^

- `https://www.jetbrains.com/pycharm/guide/playlists/42/ <https://www.jetbrains.com/pycharm/guide/playlists/42/>`_

Windows-Benutzer verwenden die Ctrl-Tast, Mac-User die  Cmd-Taste!

.. table::

    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | MacOS/Linux        | Windows                          | Objekt          | Action                                                 |
    +====================+==================================+=================+========================================================+
    | Cmd+Up             | Alt + Home                       | Dateien/Ordner  | Datei wechseln/öffnen                                  |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Cmd + e            | Ctrl + e                         | Dateien/Ordner  | Umschaltung Views und Liste zuletzt geöffnet           |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Alt-Cmd-o          | Ctrl + Umschalt + n              | Dateien/Ordner  | Goto file ...  (Tab zum wechseln der Kategorie)        |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Cmd + 1            | Alt + 1                          | Projekt-Ansicht | ein- und ausblenden                                    |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | cmd + ,            | Ctrl + Alt + s                   | Einstellungen   | \                                                      |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Alt-Cmd-o, Cmd + n | Ctrl + Umschalt + n, Alt + Einfg | Dateien/Ordner  | Goto file ... danach Datei-Neu-Dialog                  |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Shift + Enter      | Shift + Enter                    | \               | Neue Zeil unterhalb der aktuellen Zeile                |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Alt + Cmd + Enter  | Ctr + Alt + Enter                | \               | Neue Zeile vor der aktuellen Zeile                     |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Alt-Up             | Ctrl + w                         | \               | Auswahl erweitern                                      |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Shift + Cmd + a    | Shift + Ctrl + a                 | Action          | Aktionen ausführen                                     |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | \                  | \                                | Action          | tippe »spl vert« :math:`\Rightarrow` Bildschirm teilen |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Alt-Tab            | \                                | \               | Fenster wechseln, wenn zweigeteilter Bildschirm        |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+
    | Ctrl, Space, Space | \                                | import          | vervollständigen                                       |
    +--------------------+----------------------------------+-----------------+--------------------------------------------------------+

Testing auf pytest umstellen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. index:: pytest; PyCharm (Einstellungen)

PyCharm verwendet *UnitTests* als Grundeinstellung.
Für die Umstellung auf ein anderes Testsystem, findet man wie
nachfolgend gezeigt die Optionen

Wenn *pytest* noch nicht den requirements.txt enthalten ist, gibt es eine
Button »Fix«.

.. image:: images/pycharm_pytest.png

Visual Studio und Visual Studio Code
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bieten Erweiterungen für die Unterstützung von Python.

Eclipse
~~~~~~~

- über Erweiterungen

- LiClipse (mit vorinstallierten Erweiterungen)

Eric (Qt-IDE)
~~~~~~~~~~~~~

`https://eric-ide.python-projects.org/eric-download.html <https://eric-ide.python-projects.org/eric-download.html>`_

Sammlung von Lösungsansätzen (Übersicht)
----------------------------------------

.. index:: Python-Lösungen (eine Sammlung)

Das Fahrrad muss nicht immer neu erfunden werden. Sortiert nach
Kategorien, gibt es auf der folgenden Seite viele Lösungen.

`https://github.com/vinta/awesome-python <https://github.com/vinta/awesome-python>`_

Testing
-------

Quellcode der nicht getestes wurde ist fehlerhaft. Folgende Tools
stehen zur Verfügung:

- pycodestyle (pep8)

- flake8

- pytest

- nose
