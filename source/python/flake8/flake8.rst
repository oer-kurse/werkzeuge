=====================
Flake8 (Syntax-Check)
=====================


.. index:: Flake8

.. index:: Developer; Syntax-Check

.. index:: Syntax-Check

Flake8 macht aus dem Code noch besseren Code, weil oft auch nicht
sichtbare Schönheitsfehler, wie überflüssige Leerzeichen und Zeilen,
fehlende, konsistente Verwendung von Leerzeichen überflüssige Importe
u.v.a angezeigt werden.
Oft hat es keinen Einfluß auf den Quellcode, aber der Parser wird mit
unnötigen Prüfschritten belastet, was nicht sein muss und es spart in
vielen Fällen auch noch ein klein wenig Speicherplatz.

Also beginnen Sie mit der Refaktorisierung jetzt!

Installation
------------

.. code-block:: bash


    pip install flake8

Ausführung
----------

.. code-block:: bash


    # für alle Python-Dateien

    flake8

    # oder mit einer einzelnen Datei:

    flake8 meine_neue_python_datei.py

Beispielausgabe
---------------

.. code-block:: bash


    flake8 view_personen.py
    view_personen.py:1:4: W291 trailing whitespace
    view_personen.py:7:40: W291 trailing whitespace
    view_personen.py:16:1: F401 'py2neo.Relationship' imported but unused
    view_personen.py:18:1: E302 expected 2 blank lines, found 1
    view_personen.py:23:16: E221 multiple spaces before operator
    view_personen.py:30:22: E127 continuation line over-indented for visual indent
    view_personen.py:34:1: E302 expected 2 blank lines, found 1
    view_personen.py:37:12: F821 undefined name 'personwahl'
    view_personen.py:39:1: E302 expected 2 blank lines, found 1
    view_personen.py:42:45: E231 missing whitespace after ','
    view_personen.py:48:22: E127 continuation line over-indented for visual indent
    view_personen.py:51:1: W293 blank line contains whitespace
    view_personen.py:52:1: E302 expected 2 blank lines, found 1
    view_personen.py:68:1: W391 blank line at end of file

Nach der Überarbeitung
----------------------

Gibts ein Bienchen... ;-)
