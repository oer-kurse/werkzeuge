.. index:: Verlinkungen, Links, ref

.. _links1:

==============================
 Verlinkungen (Seite 1 von 2)
==============================

Verlinkung zu einer externen Website
====================================

Quelle
------
::

  # Definition (nicht sichtbar)

  .. _http://vocaword.org: http://vocaword.org

  # Anwendung (sichtbar)

  `http://vocaword.org`_
  

Anwendung
---------

`http://vocaword.org`_


.. _http://vocaword.org: http://vocaword.org


Verlinkung zu einer anderen Seite (intern)
==========================================
Referenzen, damit können Quervereise im Dokument erzeugt werden.

1. einen Zielnamen definieren...

Quelle:
::
  
   .. _links2:
   
2. Zum Ziel verweisen... (hier als Bildschirmkopie eingefügt)   

   .. image:: linkto.png
      :width: 50%

..   :ref: `Sprung zu einer anderen Seite mit dem Sprungziel *links2*<links2>`

Anwendung
---------

:ref:`Sprung zu einer anderen Seite mit dem Sprunziel *links2*<links2>`

.. index:: download

Download-Link
=============
Quelle:
::


  :download:`Alle Bilder als zip-Datei <files/linkto.png.zip>`

Ausgabe:

:download:`Alle Bilder als zip-Datei <files/linkto.png.zip>`

Link-Referenz
=============
Links sind oft sehr lang oder können zahlreich im Text verteilt sein.
Eine Möglichkeit der "Zentralen Verwaltung" an einer Stelle könnte
hilfreich sein.

1. Sammeln sie alle Links am Ende eines Dokumentes.
::

   .. _PDF zum Lehrgang in Hamburg:   /static/download/hamburg/hamburg2014.pdf
   .. _PDF zum Lehrgang in Stuttgart: /static/download/stuttgart/stuttgart2015.pdf

2. Im Text verweisen Sie dann auf diese Links:
::

   `PDF zum Lehrgang in Stuttgart`_
   `PDF zum Lehrgang in Hamburg`_

Wollen Sie das Dokument mit gleichem Link austauschen brauchen sie nur
zum Ende des Dokumentes springen und tauschen den Namen zur Datei aus.

   
