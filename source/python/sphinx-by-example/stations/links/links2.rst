.. index:: Verlinkungen, Links

.. _links2:

============================
Verlinkungen (Seite 2 von 2)
============================
:ref:`Und wieder zurück...<links1>`


Rücksprung zur vorherigen Seite
===============================

Referenzen, damit können Quervereise im Dokument erzeugt werden.

und wieder zurück zur ersten Seite, von der wir hierher gelangt sind...

:ref:`Sprung zu einer anderen Seite mit dem Sprunziel *links1*<links1>`
