===========
ToDo-Listen
===========

mit Todo-Einträgen kann man auf unvollendete Texte verweisen.

Extension einbinden in die conf.py
::

   extensions = [
    'sphinx.ext.todo',
   ]

   todo_include_todos = True

Ein Eintrag sieht wie folgt aus:
::

   .. todo:: Was noch zu tun ist....

Die Liste der Todo-Einträge inclusive eines Links zur fraglichen
Stelle kann dann wie folgt eingebunden werden.
      

