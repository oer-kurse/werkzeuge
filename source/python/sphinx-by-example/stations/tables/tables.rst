.. Sphinx-by-Example documentation master file, created by
   sphinx-quickstart on Mon Aug  4 19:24:28 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==========
 Tabellen
==========

Grid-Tabelle
============

Quelle
------
::

  +------------------+----------------+-------------------+
  |Atom              | Diameter       | Color             | 
  +==================+================+===================+
  | Hydrogen         | 0,32           | blue              |         
  +------------------+----------------+-------------------+
  | Carbon           | 0,91           | green             |         
  +------------------+----------------+-------------------+
  | Knitted fabric   | 0,75           | blue              |         
  +------------------+----------------+-------------------+
  | Phospor          | 1,06           | red               |         
  +------------------+----------------+-------------------+
  | Oxygen           | 0,73           | red               |         
  +------------------+----------------+-------------------+
  | unknown Atom     | 1,0            | black             |         
  +------------------+----------------+-------------------+

Ergebnis
--------

+------------------+----------------+-------------------+
|Atom              | Diameter       | Color             | 
+==================+================+===================+
| Hydrogen         | 0,32           | blue              |         
+------------------+----------------+-------------------+
| Carbon           | 0,91           | green             |         
+------------------+----------------+-------------------+
| Knitted fabric   | 0,75           | blue              |         
+------------------+----------------+-------------------+
| Phospor          | 1,06           | red               |         
+------------------+----------------+-------------------+
| Oxygen           | 0,73           | red               |         
+------------------+----------------+-------------------+
| unknown Atom     | 1,0            | black             |         
+------------------+----------------+-------------------+


Einfache Tabelle
================

Mit folgenden Einschränkungen:

- nur Einzeiler

Quelle
------
::

  =====  =====  =======
  A      B      A and B
  =====  =====  =======
  False  False  False
  True   False  False
  False  True   False
  True   True   True
  =====  =====  =======

Ergebnis
--------

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======

Quelle
------

::

  ===== =====
  col 1 col 2
  ===== =====
  1     Second column of row 1.
  2     Second column of row 2.
        Second line of paragraph.
  3     - Second column of row 3.
        - Second item in bullet
          list (row 3, column 2).
  4     is a span
  ===== =====

Ergebnis
--------

===== =====
col 1 col 2
===== =====
1     Second column of row 1.
2     Second column of row 2.
      Second line of paragraph.
3     - Second column of row 3.
      - Second item in bullet
        list (row 3, column 2).
4     is a span
===== =====

Quelle
------
::

  +------------------------+------------+----------+----------+
  | Header row, column 1   | Header 2   | Header 3 | Header 4 |
  +========================+============+==========+==========+
  | body row 1, column 1   | column 2   | column 3 | column 4 |
  +------------------------+------------+----------+----------+
  | body row 2             | Cells may span columns.          |
  +------------------------+------------+---------------------+
  | body row 3             | Cells may  | - Table cells       |
  +------------------------+ span rows. | - contain           |
  | body row 4             |            | - body elements.    |
  +------------------------+------------+---------------------+

Ergebnis
------

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+---------------------+


.. index:: list-tabel, table, table from a list

Von einer Liste zur Tabelle
===========================

Quelle
------
::

  .. list-table:: Frozen Delights!
     :widths: 15 10 30
     :header-rows: 1

     * - Treat
       - Quantity
       - Description
     * - Albatross
       - 2.99
       - On a stick!
     * - Crunchy Frog
       - 1.49
       - If we took the bones out, it wouldn't be
         crunchy, now would it?
     * - Gannet Ripple
       - 1.99
       - On a stick!

Ergebnis
--------

.. list-table:: Frozen Delights!
   :widths: 15 10 30
   :header-rows: 1

   * - Treat
     - Quantity
     - Description
   * - Albatross
     - 2.99
     - On a stick!
   * - Crunchy Frog
     - 1.49
     - If we took the bones out, it wouldn't be
       crunchy, now would it?
   * - Gannet Ripple
     - 1.99
     - On a stick!

Bild und Text nebeneinander
===========================

Quelle
------
::

  .. list-table::
     :widths: 15  30

     * - .. image:: images/logo.png
       - Das ist der Text zum Bild


Ergebnis
--------

.. list-table::
   :widths: 15  30

   * - .. image:: images/logo.png
     - Das ist der Text zum Bild

Excel-Tabelle einbinden
=======================

Siehe: https://pythonhosted.org/sphinxcontrib-exceltable/

