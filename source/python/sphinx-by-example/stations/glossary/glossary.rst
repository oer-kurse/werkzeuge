==========
 Glossary
==========

Definitionen, Begriffserklärungen...
====================================


Quelle:
-------
::

  .. glossary::

     cn 
        Common Name

     ou 
        Organizational Unit

     dc 
        Domain Component

Ergebnis
--------

.. glossary::

   cn 
      Common Name

   ou 
      Organizational Unit

   dc 
      Domain Component  
