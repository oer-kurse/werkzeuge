=========
Fussnoten
=========

Quelle
------

::

  Wissensmanagement
  ~~~~~~~~~~~~~~~~~

  Nonaka und Takeuchi beschreiben, dass implizites Wissen (einzelner Personen) 
  in explizites Wissen, also aufgeschriebenenes Wissen umgewandelt 
  werdem muss... [#f1]_ 

  Drucker wiederum erkannte, dass es nicht möglich ist, das Wissen aus 
  den "Köpfen der Spezialisten zu extrahieren".  [#f2]_ 


  .. [#f1] Nonaka Ikujiro 1998 -- *The Knowledg-Creating Company* in Harvard Business  
         review on Knlowledgemenagement, pages 21-45. Harvard Business Review, 1998
  .. [#f2] Peter Drucker 1988 -- *The Coming  of the new Organization* in Haward Review   


Ergebnis
--------

Wissensmanagement
~~~~~~~~~~~~~~~~~

Nonaka und Takeuchi beschreiben, dass implizites Wissen (einzelner Personen) 
in explizites Wissen, also aufgeschriebenenes Wissen umgewandelt 
werden muss... [#f1]_ 

Drucker wiederum erkannte, dass es nicht möglich ist, das Wissen aus 
den "Köpfen der Spezialisten zu extrahieren".  [#f2]_ 


.. [#f1] Nonaka Ikujiro 1998 -- *The Knowledg-Creating Company* in Harvard Business  
         review on Knlowledgemenagement, pages 21-45. Harvard Business Review, 1998
.. [#f2] Peter Drucker 1988 -- *The Coming  of the new Organization* in Haward Review   
