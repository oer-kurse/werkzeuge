.. index:: conf.py, css

============
CSS anpassen
============
In der conf.py kann eine eigene CSS-Datei eingebunden werden. Darin
ist es dann möglich CSS-Anweisungen des Templates zu überschreiben
bzw. zu ergänzen.

::

   html_context = {
    'css_files': ['_static/custom.css'],
}
