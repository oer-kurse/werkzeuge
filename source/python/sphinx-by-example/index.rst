.. Sphinx-by-Example documentation master file, created by
   sphinx-quickstart on Mon Aug  4 19:24:28 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. index:: sphinx, doku
 
Sphinx-by-Example's (Dokumentation)
===================================

**Inhalte erstellen**

.. toctree::
   :maxdepth: 2

   stations/bilder/bilder4
   stations/fussnoten/fussnoten
   stations/glossary/glossary
   stations/math/math
   stations/links/links1
   stations/links/links2
   stations/scripts/scripts
   stations/tables/tables
   stations/todos/todoliste
   stations/videos/videos

**Konfiguration**

.. toctree::
   :maxdepth: 2
   
   stations/templating/eigenes-css
