=================================
venv (isolierte Entwicklerordner)
=================================


.. index:: Tools

.. index:: Werkzeuge, venv, env

Virtuelle Umgebungen einrichten
-------------------------------

.. index:: venv (Mac/Linux)

.. index:: virtuelle Umgebungen (Mac/Linux)

Einmalig pro Projekt
~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash


    cd entwicklerordner
    python -m venv env
    cd projektname

.. index:: Virtuelle Umgebung; aktivieren

.. index:: activate; Mac/Linux

Immer vor der Arbeit am Projekt (Mac/Linux)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash


    cd entwicklerordner/projektname
    source ./env/bin/activate

.. index:: activate; Windows

Immer vor der Arbeit am Projekt (Windows)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. index:: venv (Windows)

.. index:: virtuelle Umgebungen (Windows)

.. code-block:: bash


    cd entwicklerordner\projektname
    source .\Scripts\activate
