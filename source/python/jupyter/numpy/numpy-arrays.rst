
NumPy: Arrays
=============

http://docs.scipy.org/doc/numpy/reference/

.. index:: NumPy; arrays
.. index:: Arrray; NumPy

.. code:: ipython3

    a = [1, 2]
    2*a




.. parsed-literal::

    [1, 2, 1, 2]



.. code:: ipython3

    a = [1, 2]
    b = [3, 4]

.. code:: ipython3

    c = a + b
    c




.. parsed-literal::

    [1, 2, 3, 4]



.. code:: ipython3

    import numpy as np
    
    a = np.array([1, 2])
    print(2*a)
    b = np.array([3, 4])
    print(a + b)
    print(type(a))
    print(np.shape(a))
    print(np.shape(c))



.. parsed-literal::

    [2 4]
    [4 6]
    <class 'numpy.ndarray'>
    (2,)
    (4,)


.. code:: ipython3

    x =  np.array(range(5))
    print(x)


.. parsed-literal::

    [0 1 2 3 4]


.. code:: ipython3

    # explizite Typ-Angabe
    np.array([1, 2, 3], dtype=complex)




.. parsed-literal::

    array([1.+0.j, 2.+0.j, 3.+0.j])



.. code:: ipython3

    # Mehrdimensional arrays
    meineListe = [[1,2,3],[4,5,6]]
    meinArray = np.array(meineListe)
    print(meinArray)
    np.shape(meinArray)


.. parsed-literal::

    [[1 2 3]
     [4 5 6]]




.. parsed-literal::

    (2, 3)



.. code:: ipython3

    type(meinArray)
    # ndaray steht für: N-dimensional array




.. parsed-literal::

    numpy.ndarray



.. code:: ipython3

    # Array initalisieren mit 0
    nullArray = np.zeros((2,4))
    print(nullArray)


.. parsed-literal::

    [[0. 0. 0. 0.]
     [0. 0. 0. 0.]]


.. code:: ipython3

    # Array initalisieren mit 1
    einserArray = np.ones((2,4))
    print(einserArray)


.. parsed-literal::

    [[1. 1. 1. 1.]
     [1. 1. 1. 1.]]


.. code:: ipython3

    # Array initalisieren mit Zufallswerten
    randomArray = np.empty([2, 3])
    print(randomArray)


.. parsed-literal::

    [[0. 0. 0.]
     [0. 0. 0.]]


.. code:: ipython3

    # Array initalisieren mit Zufallswerten
    np.empty([2, 2])




.. parsed-literal::

    array([[4.9e-324, 9.9e-324],
           [1.5e-323, 2.0e-323]])



.. code:: ipython3

    # numpy.arange([start], stop[, step], dtype=None)
    
    a = np.arange(5, 10, 0.5)
    print(a)


.. parsed-literal::

    [5.  5.5 6.  6.5 7.  7.5 8.  8.5 9.  9.5]


.. code:: ipython3

    # Array initalisieren mit Werten zw. 0 to 1
    randomArray2 = np.random.random((4,3))
    print(randomArray2)


.. parsed-literal::

    [[0.0067767  0.45884586 0.43208584]
     [0.15784792 0.97948398 0.5203546 ]
     [0.31923238 0.79828092 0.33487702]
     [0.99323566 0.19165176 0.67402207]]


.. code:: ipython3

    def pythonsum(n):
        
        a = list(range(n))
        b = list(range(n))
        c = []
    
        for i in range(len(a)):
            a[i] = i ** 2
            b[i] = i ** 3
            c.append(a[i] + b[i])
    
        return c

.. code:: ipython3

    print(pythonsum(10))


.. parsed-literal::

    [0, 2, 12, 36, 80, 150, 252, 392, 576, 810]


.. code:: ipython3

    def numpysum(n):
      a = np.arange(n) ** 2
      b = np.arange(n) ** 3
      c = a + b
      return c

.. code:: ipython3

    print(numpysum(10))


.. parsed-literal::

    [  0   2  12  36  80 150 252 392 576 810]


.. code:: ipython3

    # einzeilig
    print(np.arange(1,50))
    # mehrzeilig
    7*7
    multidim = np.arange(1,50).reshape((7,7))
    print(multidim)
    #print(multidim.shape)


.. parsed-literal::

    [ 1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
     25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48
     49]
    [[ 1  2  3  4  5  6  7]
     [ 8  9 10 11 12 13 14]
     [15 16 17 18 19 20 21]
     [22 23 24 25 26 27 28]
     [29 30 31 32 33 34 35]
     [36 37 38 39 40 41 42]
     [43 44 45 46 47 48 49]]


.. code:: ipython3

    # Umformungen
    lotto = multidim.copy()
    lotto.flatten() # speichert Ergebnis in neuen Speicherbereich
    lotto.ravel()   # nutzt gleichen Speicher
    lotto.T         # transpose 




.. parsed-literal::

    array([[ 1,  8, 15, 22, 29, 36, 43],
           [ 2,  9, 16, 23, 30, 37, 44],
           [ 3, 10, 17, 24, 31, 38, 45],
           [ 4, 11, 18, 25, 32, 39, 46],
           [ 5, 12, 19, 26, 33, 40, 47],
           [ 6, 13, 20, 27, 34, 41, 48],
           [ 7, 14, 21, 28, 35, 42, 49]])



.. code:: ipython3

    # Zugriff auf Werte und Teile
    from random import randint
    for tip in range(6):
        print(multidim[randint(0,6),randint(0,6)])
    #multidim[4,3]


.. parsed-literal::

    47
    3
    26
    1
    43
    11


.. code:: ipython3

    multidim[:,3:]




.. parsed-literal::

    array([[ 4,  5,  6,  7],
           [11, 12, 13, 14],
           [18, 19, 20, 21],
           [25, 26, 27, 28],
           [32, 33, 34, 35],
           [39, 40, 41, 42],
           [46, 47, 48, 49]])



.. code:: ipython3

    durchschnitt = np.average(multidim)
    durchschnitt
    ueberdurchschnittlich = multidim > durchschnitt
    ueberdurchschnittlich




.. parsed-literal::

    array([[False, False, False, False, False, False, False],
           [False, False, False, False, False, False, False],
           [False, False, False, False, False, False, False],
           [False, False, False, False,  True,  True,  True],
           [ True,  True,  True,  True,  True,  True,  True],
           [ True,  True,  True,  True,  True,  True,  True],
           [ True,  True,  True,  True,  True,  True,  True]])



.. code:: ipython3

    alle_ueber_druchschnitt = multidim[ueberdurchschnittlich]
    alle_ueber_druchschnitt




.. parsed-literal::

    array([26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
           43, 44, 45, 46, 47, 48, 49])



.. code:: ipython3

    standardAbweichung = np.std(multidim)
    print(standardAbweichung)


.. parsed-literal::

    14.142135623730951


.. code:: ipython3

    # Multiplikation
    
    null_array = multidim * np.zeros((7,7))
    null_array




.. parsed-literal::

    array([[0., 0., 0., 0., 0., 0., 0.],
           [0., 0., 0., 0., 0., 0., 0.],
           [0., 0., 0., 0., 0., 0., 0.],
           [0., 0., 0., 0., 0., 0., 0.],
           [0., 0., 0., 0., 0., 0., 0.],
           [0., 0., 0., 0., 0., 0., 0.],
           [0., 0., 0., 0., 0., 0., 0.]])



.. code:: ipython3

    # Addition
    
    alles_plus_eins = multidim + np.ones((7,7))
    alles_plus_eins




.. parsed-literal::

    array([[ 2.,  3.,  4.,  5.,  6.,  7.,  8.],
           [ 9., 10., 11., 12., 13., 14., 15.],
           [16., 17., 18., 19., 20., 21., 22.],
           [23., 24., 25., 26., 27., 28., 29.],
           [30., 31., 32., 33., 34., 35., 36.],
           [37., 38., 39., 40., 41., 42., 43.],
           [44., 45., 46., 47., 48., 49., 50.]])



.. code:: ipython3

    np.append(alles_plus_eins, [51,52,53])




.. parsed-literal::

    array([ 2.,  3.,  4.,  5.,  6.,  7.,  8.,  9., 10., 11., 12., 13., 14.,
           15., 16., 17., 18., 19., 20., 21., 22., 23., 24., 25., 26., 27.,
           28., 29., 30., 31., 32., 33., 34., 35., 36., 37., 38., 39., 40.,
           41., 42., 43., 44., 45., 46., 47., 48., 49., 50., 51., 52., 53.])



.. code:: ipython3

    # Typecast
    alles_plus_eins.astype(int)




.. parsed-literal::

    array([[ 2,  3,  4,  5,  6,  7,  8],
           [ 9, 10, 11, 12, 13, 14, 15],
           [16, 17, 18, 19, 20, 21, 22],
           [23, 24, 25, 26, 27, 28, 29],
           [30, 31, 32, 33, 34, 35, 36],
           [37, 38, 39, 40, 41, 42, 43],
           [44, 45, 46, 47, 48, 49, 50]])


