
Pandas: Dataframe/Stringmanipulationen
======================================

.. index:: Pandas: strings

.. code:: ipython3

    import pandas as pd

.. code:: ipython3

    df = pd.read_csv('./lwzweit2019.csv', delimiter=";", encoding = "ISO-8859-1")
    df.head()
    df.shape




.. parsed-literal::

    (217, 19)



.. code:: ipython3

    df.Name.str.contains('Pots')




.. parsed-literal::

    0      False
    1      False
    2      False
    3      False
    4      False
           ...  
    212    False
    213    False
    214     True
    215     True
    216     True
    Name: Name, Length: 217, dtype: bool



.. code:: ipython3

    df.Name.str.upper()




.. parsed-literal::

    0                        BORNSTEDT - FLIEDERWEG
    1             BORNSTEDT - KIRSCHALLEE/THAERSTR.
    2      BORNSTEDT - A.-WOLFF-PLATZ/E.-ARENDT-STR
    3                                        SACROW
    4            EICHE - KAISER-FRIEDRICH-STR. SÃ¼D
                             ...                   
    212       DREWITZ - ALT DREWITZ/AM SILBERGRABEN
    213                KIRCHSTEIGFELD - PRIESTERWEG
    214                POTSDAM BRIEFWAHLBEZIRK 9065
    215                POTSDAM BRIEFWAHLBEZIRK 9070
    216                POTSDAM BRIEFWAHLBEZIRK 9071
    Name: Name, Length: 217, dtype: object



.. code:: ipython3

    df.Name.str.lstrip()




.. parsed-literal::

    0                        Bornstedt - Fliederweg
    1             Bornstedt - Kirschallee/Thaerstr.
    2      Bornstedt - A.-Wolff-Platz/E.-Arendt-Str
    3                                        Sacrow
    4            Eiche - Kaiser-Friedrich-Str. SÃ¼d
                             ...                   
    212       Drewitz - Alt Drewitz/Am Silbergraben
    213                Kirchsteigfeld - Priesterweg
    214                Potsdam Briefwahlbezirk 9065
    215                Potsdam Briefwahlbezirk 9070
    216                Potsdam Briefwahlbezirk 9071
    Name: Name, Length: 217, dtype: object



.. code:: ipython3

    df.Name.str.replace(' ', '').head()




.. parsed-literal::

    0                      Bornstedt-Fliederweg
    1           Bornstedt-Kirschallee/Thaerstr.
    2    Bornstedt-A.-Wolff-Platz/E.-Arendt-Str
    3                                    Sacrow
    4           Eiche-Kaiser-Friedrich-Str.SÃ¼d
    Name: Name, dtype: object


