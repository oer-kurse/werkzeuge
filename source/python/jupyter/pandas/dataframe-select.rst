
Pandas: Daten auswählen
=======================

.. index:: Pandas: Daten auswählen

.. code:: ipython3

    import pandas as pd

.. code:: ipython3

    df = pd.read_csv('./lwzweit2019.csv', delimiter=";", encoding = "ISO-8859-1")
    df.head()
    df.shape




.. parsed-literal::

    (217, 19)



.. code:: ipython3

    spalte = df['Wahlkreis'] 
    print(type(spalte))
    print(spalte)


.. parsed-literal::

    <class 'pandas.core.series.Series'>
    0      19
    1      19
    2      19
    3      19
    4      19
           ..
    212    22
    213    22
    214    22
    215    22
    216    22
    Name: Wahlkreis, Length: 217, dtype: int64


.. code:: ipython3

    zweispalten = df[['Wahlkreis', 'Name']] 
    print(type(zweispalten))
    print(zweispalten.head())


.. parsed-literal::

    <class 'pandas.core.frame.DataFrame'>
       Wahlkreis                                      Name
    0         19                    Bornstedt - Fliederweg
    1         19         Bornstedt - Kirschallee/Thaerstr.
    2         19  Bornstedt - A.-Wolff-Platz/E.-Arendt-Str
    3         19                                    Sacrow
    4         19        Eiche - Kaiser-Friedrich-Str. SÃ¼d


.. code:: ipython3

    # Punkt-Schreibweise
    zweispalten.Name




.. parsed-literal::

    0                        Bornstedt - Fliederweg
    1             Bornstedt - Kirschallee/Thaerstr.
    2      Bornstedt - A.-Wolff-Platz/E.-Arendt-Str
    3                                        Sacrow
    4            Eiche - Kaiser-Friedrich-Str. SÃ¼d
                             ...                   
    212       Drewitz - Alt Drewitz/Am Silbergraben
    213                Kirchsteigfeld - Priesterweg
    214                Potsdam Briefwahlbezirk 9065
    215                Potsdam Briefwahlbezirk 9070
    216                Potsdam Briefwahlbezirk 9071
    Name: Name, Length: 217, dtype: object



.. code:: ipython3

    # Zugriff mit loc
    df.loc[:,'Name']
    df.loc[4,'Name']





.. parsed-literal::

    'Eiche - Kaiser-Friedrich-Str. SÃ¼d'



.. code:: ipython3

    # Zugriff mit iloc
    df.iloc[4,3]




.. parsed-literal::

    757



.. code:: ipython3

    df.iloc[4,:]




.. parsed-literal::

    Nr                                                     1502
    Wahlkreis                                                19
    Name                     Eiche - Kaiser-Friedrich-Str. SÃ¼d
    Wahlb. insges.                                          757
    WÃ¤hler                                                 335
    UngÃ¼lt. Zweitstimmen                                     3
    GÃ¼ltige Zweitstimmen                                   332
    SPD                                                      82
    CDU                                                      40
    DIE LINKE                                                51
    AfD                                                      68
    GRÃNE/B 90                                              55
    BVB / FREIE WÃHLER                                       8
    PIRATEN                                                   1
    FDP                                                      14
    ÃDP                                                      4
    Tierschutzpartei                                          8
    V-ParteiÂ³                                                1
    Sonstige                                                  0
    Name: 4, dtype: object



.. code:: ipython3

    df.iloc[4,8:12]




.. parsed-literal::

    CDU            40
    DIE LINKE      51
    AfD            68
    GRÃNE/B 90    55
    Name: 4, dtype: object



.. code:: ipython3

    # Sortierung
    df.sort_values('Name').head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Nr</th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>WÃ¤hler</th>
          <th>UngÃ¼lt. Zweitstimmen</th>
          <th>GÃ¼ltige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÃNE/B 90</th>
          <th>BVB / FREIE WÃHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÃDP</th>
          <th>Tierschutzpartei</th>
          <th>V-ParteiÂ³</th>
          <th>Sonstige</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>43</th>
          <td>5206</td>
          <td>21</td>
          <td>Babelsberg Nord - Alt Nowawes West</td>
          <td>1067</td>
          <td>610</td>
          <td>4</td>
          <td>606</td>
          <td>160</td>
          <td>50</td>
          <td>91</td>
          <td>57</td>
          <td>186</td>
          <td>10</td>
          <td>10</td>
          <td>19</td>
          <td>6</td>
          <td>11</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>41</th>
          <td>5203</td>
          <td>21</td>
          <td>Babelsberg Nord - Fontanestr.</td>
          <td>1255</td>
          <td>690</td>
          <td>0</td>
          <td>690</td>
          <td>165</td>
          <td>92</td>
          <td>74</td>
          <td>55</td>
          <td>217</td>
          <td>10</td>
          <td>3</td>
          <td>60</td>
          <td>2</td>
          <td>11</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>156</th>
          <td>5208</td>
          <td>21</td>
          <td>Babelsberg Nord - Goetheplatz</td>
          <td>1140</td>
          <td>705</td>
          <td>8</td>
          <td>697</td>
          <td>176</td>
          <td>78</td>
          <td>86</td>
          <td>62</td>
          <td>229</td>
          <td>13</td>
          <td>3</td>
          <td>31</td>
          <td>5</td>
          <td>11</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>143</th>
          <td>5202</td>
          <td>21</td>
          <td>Babelsberg Nord - Karl-Marx-Str./Donarst</td>
          <td>1160</td>
          <td>646</td>
          <td>5</td>
          <td>641</td>
          <td>149</td>
          <td>117</td>
          <td>57</td>
          <td>70</td>
          <td>161</td>
          <td>10</td>
          <td>4</td>
          <td>53</td>
          <td>9</td>
          <td>7</td>
          <td>4</td>
          <td>0</td>
        </tr>
        <tr>
          <th>141</th>
          <td>5201</td>
          <td>21</td>
          <td>Babelsberg Nord - Klein Glienicke</td>
          <td>960</td>
          <td>520</td>
          <td>3</td>
          <td>517</td>
          <td>128</td>
          <td>56</td>
          <td>43</td>
          <td>36</td>
          <td>173</td>
          <td>9</td>
          <td>5</td>
          <td>56</td>
          <td>2</td>
          <td>8</td>
          <td>1</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

     df.sort_values('Name', ascending=False).head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Nr</th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>WÃ¤hler</th>
          <th>UngÃ¼lt. Zweitstimmen</th>
          <th>GÃ¼ltige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÃNE/B 90</th>
          <th>BVB / FREIE WÃHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÃDP</th>
          <th>Tierschutzpartei</th>
          <th>V-ParteiÂ³</th>
          <th>Sonstige</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>57</th>
          <td>4203</td>
          <td>22</td>
          <td>Zentrum Ost - Lotte-Pulewka-Str.</td>
          <td>1027</td>
          <td>458</td>
          <td>11</td>
          <td>447</td>
          <td>132</td>
          <td>23</td>
          <td>110</td>
          <td>92</td>
          <td>52</td>
          <td>5</td>
          <td>5</td>
          <td>8</td>
          <td>5</td>
          <td>15</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>53</th>
          <td>4201</td>
          <td>22</td>
          <td>Zentrum Ost - Humboldtring/Babelsb. Str.</td>
          <td>1094</td>
          <td>530</td>
          <td>5</td>
          <td>525</td>
          <td>127</td>
          <td>49</td>
          <td>79</td>
          <td>75</td>
          <td>115</td>
          <td>11</td>
          <td>6</td>
          <td>31</td>
          <td>8</td>
          <td>22</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>159</th>
          <td>4204</td>
          <td>22</td>
          <td>Zentrum Ost - Humboldtring Ost/Wiesenstr</td>
          <td>1379</td>
          <td>647</td>
          <td>11</td>
          <td>636</td>
          <td>243</td>
          <td>49</td>
          <td>116</td>
          <td>106</td>
          <td>71</td>
          <td>11</td>
          <td>8</td>
          <td>12</td>
          <td>4</td>
          <td>16</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>55</th>
          <td>4202</td>
          <td>22</td>
          <td>Zentrum Ost - Hans-Marchwitza-Ring</td>
          <td>913</td>
          <td>358</td>
          <td>10</td>
          <td>348</td>
          <td>84</td>
          <td>21</td>
          <td>69</td>
          <td>64</td>
          <td>68</td>
          <td>8</td>
          <td>4</td>
          <td>6</td>
          <td>6</td>
          <td>14</td>
          <td>4</td>
          <td>0</td>
        </tr>
        <tr>
          <th>24</th>
          <td>1507</td>
          <td>19</td>
          <td>Werder (Havel), Zur alten Weberei/Angler</td>
          <td>1054</td>
          <td>559</td>
          <td>4</td>
          <td>555</td>
          <td>123</td>
          <td>102</td>
          <td>44</td>
          <td>115</td>
          <td>94</td>
          <td>15</td>
          <td>7</td>
          <td>31</td>
          <td>7</td>
          <td>14</td>
          <td>3</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    ergebnis_filter_series = df['SPD'] > 0 
    ergebnis_filter_series





.. parsed-literal::

    0      True
    1      True
    2      True
    3      True
    4      True
           ... 
    212    True
    213    True
    214    True
    215    True
    216    True
    Name: SPD, Length: 217, dtype: bool



.. code:: ipython3

    ergebnis_filter_series = [(df['SPD'] > 10) & (df['SPD'] < 50)]
    ergebnis_filter_series




.. parsed-literal::

    [0      False
     1      False
     2      False
     3       True
     4      False
            ...  
     212    False
     213    False
     214    False
     215    False
     216    False
     Name: SPD, Length: 217, dtype: bool]



.. code:: ipython3

    # ???
    df['Name'].isin(['Potsdam'])




.. parsed-literal::

    0      False
    1      False
    2      False
    3      False
    4      False
           ...  
    212    False
    213    False
    214    False
    215    False
    216    False
    Name: Name, Length: 217, dtype: bool



.. code:: ipython3

    df.axes




.. parsed-literal::

    [RangeIndex(start=0, stop=217, step=1),
     Index(['Nr', 'Wahlkreis', 'Name', 'Wahlb. insges.', 'WÃ¤hler',
            'UngÃ¼lt. Zweitstimmen', 'GÃ¼ltige Zweitstimmen', 'SPD', 'CDU',
            'DIE LINKE', 'AfD', 'GRÃNE/B 90', 'BVB / FREIE WÃHLER', 'PIRATEN',
            'FDP', 'ÃDP', 'Tierschutzpartei', 'V-ParteiÂ³', 'Sonstige'],
           dtype='object')]



.. code:: ipython3

    # ???
    # axis0 = Zeilen oder 'rows'
    # axis1 = Spalten oder 'columns'
    #df.mean(axis=1) 

.. code:: ipython3

    df.head()
    df.drop('Sonstige', axis=1).head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Nr</th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>WÃ¤hler</th>
          <th>UngÃ¼lt. Zweitstimmen</th>
          <th>GÃ¼ltige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÃNE/B 90</th>
          <th>BVB / FREIE WÃHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÃDP</th>
          <th>Tierschutzpartei</th>
          <th>V-ParteiÂ³</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1302</td>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1304</td>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1309</td>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1401</td>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1502</td>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. SÃ¼d</td>
          <td>757</td>
          <td>335</td>
          <td>3</td>
          <td>332</td>
          <td>82</td>
          <td>40</td>
          <td>51</td>
          <td>68</td>
          <td>55</td>
          <td>8</td>
          <td>1</td>
          <td>14</td>
          <td>4</td>
          <td>8</td>
          <td>1</td>
        </tr>
      </tbody>
    </table>
    </div>


