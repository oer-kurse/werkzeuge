
Pandas: DataFrame
=================

.. code:: ipython3

    import pandas as pd

.. index:: Pandas; DataFrame (Manipulationen)

.. code:: ipython3

    df = pd.read_csv('./lwzweit2019.csv', delimiter=";")
    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Nr</th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>Wähler</th>
          <th>Ungült. Zweitstimmen</th>
          <th>Gültige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÜNE/B 90</th>
          <th>BVB / FREIE WÄHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÖDP</th>
          <th>Tierschutzpartei</th>
          <th>V-Partei³</th>
          <th>Sonstige</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1302</td>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1304</td>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1309</td>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1401</td>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1502</td>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
          <td>757</td>
          <td>335</td>
          <td>3</td>
          <td>332</td>
          <td>82</td>
          <td>40</td>
          <td>51</td>
          <td>68</td>
          <td>55</td>
          <td>8</td>
          <td>1</td>
          <td>14</td>
          <td>4</td>
          <td>8</td>
          <td>1</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    # Eine Kopie mit anderer Spalte als Index
    df.set_index('Nr').head()
    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Nr</th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>Wähler</th>
          <th>Ungült. Zweitstimmen</th>
          <th>Gültige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÜNE/B 90</th>
          <th>BVB / FREIE WÄHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÖDP</th>
          <th>Tierschutzpartei</th>
          <th>V-Partei³</th>
          <th>Sonstige</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1302</td>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1304</td>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1309</td>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1401</td>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1502</td>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
          <td>757</td>
          <td>335</td>
          <td>3</td>
          <td>332</td>
          <td>82</td>
          <td>40</td>
          <td>51</td>
          <td>68</td>
          <td>55</td>
          <td>8</td>
          <td>1</td>
          <td>14</td>
          <td>4</td>
          <td>8</td>
          <td>1</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    # ohne inplace wird eine Kopie angelegt
    df.set_index('Nr', inplace=True)
    df.head()





.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>Wähler</th>
          <th>Ungült. Zweitstimmen</th>
          <th>Gültige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÜNE/B 90</th>
          <th>BVB / FREIE WÄHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÖDP</th>
          <th>Tierschutzpartei</th>
          <th>V-Partei³</th>
          <th>Sonstige</th>
        </tr>
        <tr>
          <th>Nr</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1302</th>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1304</th>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1309</th>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1401</th>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1502</th>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
          <td>757</td>
          <td>335</td>
          <td>3</td>
          <td>332</td>
          <td>82</td>
          <td>40</td>
          <td>51</td>
          <td>68</td>
          <td>55</td>
          <td>8</td>
          <td>1</td>
          <td>14</td>
          <td>4</td>
          <td>8</td>
          <td>1</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    list(df)
    df.rename(columns = {'Wahlkreis': 'WK',
        'Wahlb. insges.':'wahlb',
        'Wähler':'Wähler',
        'Ungült. Zweitstimmen': 'uWS',
        'Gültige Zweitstimmen': 'gWS',
        'DIE LINKE': 'LINKE',
        'GRÜNE/B 90':'Grüne',
        'BVB / FREIE WÄHLER': 'Freie',
        'ÖDP':'ADP',
        'Tierschutzpartei': 'Tierschutz',
        'V-Partei³': 'V-Partei',
        'Unnamed: 20': 'leer'}).head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>WK</th>
          <th>Name</th>
          <th>wahlb</th>
          <th>Wähler</th>
          <th>uWS</th>
          <th>gWS</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>LINKE</th>
          <th>AfD</th>
          <th>Grüne</th>
          <th>Freie</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ADP</th>
          <th>Tierschutz</th>
          <th>V-Partei</th>
          <th>Sonstige</th>
        </tr>
        <tr>
          <th>Nr</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1302</th>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1304</th>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1309</th>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1401</th>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1502</th>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
          <td>757</td>
          <td>335</td>
          <td>3</td>
          <td>332</td>
          <td>82</td>
          <td>40</td>
          <td>51</td>
          <td>68</td>
          <td>55</td>
          <td>8</td>
          <td>1</td>
          <td>14</td>
          <td>4</td>
          <td>8</td>
          <td>1</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    #grouped_data = df[['CDU', 'SPD']].groupby('SPD').sum()
    grouped_data = df[['CDU', 'SPD']]
    grouped_data[0:10]
    #grouped_data




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>CDU</th>
          <th>SPD</th>
        </tr>
        <tr>
          <th>Nr</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1302</th>
          <td>76</td>
          <td>175</td>
        </tr>
        <tr>
          <th>1304</th>
          <td>111</td>
          <td>176</td>
        </tr>
        <tr>
          <th>1309</th>
          <td>108</td>
          <td>196</td>
        </tr>
        <tr>
          <th>1401</th>
          <td>12</td>
          <td>16</td>
        </tr>
        <tr>
          <th>1502</th>
          <td>40</td>
          <td>82</td>
        </tr>
        <tr>
          <th>1503</th>
          <td>86</td>
          <td>197</td>
        </tr>
        <tr>
          <th>1601</th>
          <td>31</td>
          <td>52</td>
        </tr>
        <tr>
          <th>1701</th>
          <td>61</td>
          <td>101</td>
        </tr>
        <tr>
          <th>1702</th>
          <td>103</td>
          <td>135</td>
        </tr>
        <tr>
          <th>8101</th>
          <td>18</td>
          <td>51</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    df.count()




.. parsed-literal::

    Wahlkreis               217
    Name                    217
    Wahlb. insges.          217
    Wähler                  217
    Ungült. Zweitstimmen    217
    Gültige Zweitstimmen    217
    SPD                     217
    CDU                     217
    DIE LINKE               217
    AfD                     217
    GRÜNE/B 90              217
    BVB / FREIE WÄHLER      217
    PIRATEN                 217
    FDP                     217
    ÖDP                     217
    Tierschutzpartei        217
    V-Partei³               217
    Sonstige                217
    dtype: int64



.. code:: ipython3

    #df.drop('Unnamed: 20', axis=1, inplace=True)
    #df.drop('Unnamed: 0', axis=1, inplace=True)
    list(df)
    df.dropna()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>Wähler</th>
          <th>Ungült. Zweitstimmen</th>
          <th>Gültige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÜNE/B 90</th>
          <th>BVB / FREIE WÄHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÖDP</th>
          <th>Tierschutzpartei</th>
          <th>V-Partei³</th>
          <th>Sonstige</th>
        </tr>
        <tr>
          <th>Nr</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1302</th>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1304</th>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1309</th>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1401</th>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1502</th>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
          <td>757</td>
          <td>335</td>
          <td>3</td>
          <td>332</td>
          <td>82</td>
          <td>40</td>
          <td>51</td>
          <td>68</td>
          <td>55</td>
          <td>8</td>
          <td>1</td>
          <td>14</td>
          <td>4</td>
          <td>8</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1503</th>
          <td>19</td>
          <td>Eiche - Altes Rad West/Lindstedter Str.</td>
          <td>1190</td>
          <td>641</td>
          <td>5</td>
          <td>636</td>
          <td>197</td>
          <td>86</td>
          <td>64</td>
          <td>106</td>
          <td>134</td>
          <td>13</td>
          <td>6</td>
          <td>21</td>
          <td>3</td>
          <td>6</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1601</th>
          <td>19</td>
          <td>Grube</td>
          <td>345</td>
          <td>210</td>
          <td>3</td>
          <td>207</td>
          <td>52</td>
          <td>31</td>
          <td>18</td>
          <td>37</td>
          <td>42</td>
          <td>13</td>
          <td>0</td>
          <td>7</td>
          <td>2</td>
          <td>4</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1701</th>
          <td>19</td>
          <td>Golm - Ehrenpfortenbergstr.</td>
          <td>786</td>
          <td>450</td>
          <td>6</td>
          <td>444</td>
          <td>101</td>
          <td>61</td>
          <td>58</td>
          <td>69</td>
          <td>94</td>
          <td>19</td>
          <td>4</td>
          <td>23</td>
          <td>4</td>
          <td>8</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1702</th>
          <td>19</td>
          <td>Golm - Geiselbergstr./Am Mühlenberg</td>
          <td>1004</td>
          <td>561</td>
          <td>4</td>
          <td>557</td>
          <td>135</td>
          <td>103</td>
          <td>55</td>
          <td>103</td>
          <td>94</td>
          <td>19</td>
          <td>5</td>
          <td>18</td>
          <td>9</td>
          <td>14</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>8101</th>
          <td>19</td>
          <td>Uetz-Paaren</td>
          <td>358</td>
          <td>180</td>
          <td>4</td>
          <td>176</td>
          <td>51</td>
          <td>18</td>
          <td>14</td>
          <td>42</td>
          <td>17</td>
          <td>13</td>
          <td>2</td>
          <td>9</td>
          <td>1</td>
          <td>9</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>8501</th>
          <td>19</td>
          <td>Neu Fahrland</td>
          <td>1281</td>
          <td>671</td>
          <td>2</td>
          <td>669</td>
          <td>155</td>
          <td>121</td>
          <td>72</td>
          <td>134</td>
          <td>104</td>
          <td>21</td>
          <td>7</td>
          <td>34</td>
          <td>4</td>
          <td>17</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>8602</th>
          <td>19</td>
          <td>Groß Glienicke - Bullenwinkel/Braumannwe</td>
          <td>1085</td>
          <td>576</td>
          <td>6</td>
          <td>570</td>
          <td>165</td>
          <td>103</td>
          <td>35</td>
          <td>78</td>
          <td>115</td>
          <td>20</td>
          <td>3</td>
          <td>33</td>
          <td>1</td>
          <td>17</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>8604</th>
          <td>19</td>
          <td>Groß Glienicke - An der Kirche</td>
          <td>917</td>
          <td>377</td>
          <td>3</td>
          <td>374</td>
          <td>89</td>
          <td>48</td>
          <td>40</td>
          <td>79</td>
          <td>62</td>
          <td>21</td>
          <td>4</td>
          <td>19</td>
          <td>1</td>
          <td>11</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9032</th>
          <td>19</td>
          <td>Potsdam Briefwahlbezirk 9032</td>
          <td>0</td>
          <td>773</td>
          <td>3</td>
          <td>770</td>
          <td>155</td>
          <td>162</td>
          <td>95</td>
          <td>70</td>
          <td>199</td>
          <td>22</td>
          <td>6</td>
          <td>42</td>
          <td>5</td>
          <td>11</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9072</th>
          <td>19</td>
          <td>Potsdam Briefwahlbezirk 9072</td>
          <td>0</td>
          <td>565</td>
          <td>6</td>
          <td>559</td>
          <td>142</td>
          <td>106</td>
          <td>63</td>
          <td>83</td>
          <td>109</td>
          <td>15</td>
          <td>5</td>
          <td>13</td>
          <td>5</td>
          <td>15</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9074</th>
          <td>19</td>
          <td>Potsdam Briefwahlbezirk 9074</td>
          <td>0</td>
          <td>664</td>
          <td>4</td>
          <td>660</td>
          <td>138</td>
          <td>135</td>
          <td>53</td>
          <td>83</td>
          <td>141</td>
          <td>34</td>
          <td>7</td>
          <td>52</td>
          <td>4</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1202</th>
          <td>19</td>
          <td>Schwielowsee, OT Caputh, Mehrzweckraum</td>
          <td>1495</td>
          <td>802</td>
          <td>7</td>
          <td>795</td>
          <td>213</td>
          <td>96</td>
          <td>64</td>
          <td>131</td>
          <td>165</td>
          <td>43</td>
          <td>6</td>
          <td>52</td>
          <td>4</td>
          <td>21</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1204</th>
          <td>19</td>
          <td>Schwielowsee, OT Ferch, Rathaus</td>
          <td>780</td>
          <td>360</td>
          <td>4</td>
          <td>356</td>
          <td>94</td>
          <td>40</td>
          <td>25</td>
          <td>53</td>
          <td>62</td>
          <td>57</td>
          <td>2</td>
          <td>15</td>
          <td>3</td>
          <td>5</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1205</th>
          <td>19</td>
          <td>Schwielowsee, OT Ferch, Sportlerheim</td>
          <td>768</td>
          <td>460</td>
          <td>5</td>
          <td>455</td>
          <td>93</td>
          <td>74</td>
          <td>36</td>
          <td>93</td>
          <td>67</td>
          <td>67</td>
          <td>2</td>
          <td>12</td>
          <td>1</td>
          <td>10</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1208</th>
          <td>19</td>
          <td>Schwielowsee, OT Geltow, Bürgerclub</td>
          <td>660</td>
          <td>385</td>
          <td>0</td>
          <td>385</td>
          <td>96</td>
          <td>68</td>
          <td>52</td>
          <td>59</td>
          <td>57</td>
          <td>15</td>
          <td>1</td>
          <td>27</td>
          <td>0</td>
          <td>6</td>
          <td>4</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9005</th>
          <td>19</td>
          <td>Schwielowsee, Briefwahlbezirk 9005</td>
          <td>0</td>
          <td>742</td>
          <td>3</td>
          <td>739</td>
          <td>180</td>
          <td>138</td>
          <td>97</td>
          <td>88</td>
          <td>128</td>
          <td>59</td>
          <td>1</td>
          <td>33</td>
          <td>0</td>
          <td>13</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9006</th>
          <td>19</td>
          <td>Schwielowsee, Briefwahlbezirk 9006</td>
          <td>0</td>
          <td>1048</td>
          <td>4</td>
          <td>1044</td>
          <td>248</td>
          <td>156</td>
          <td>115</td>
          <td>129</td>
          <td>210</td>
          <td>85</td>
          <td>6</td>
          <td>62</td>
          <td>6</td>
          <td>24</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1500</th>
          <td>19</td>
          <td>Werder (Havel), Kita Anne Frank</td>
          <td>1054</td>
          <td>525</td>
          <td>7</td>
          <td>518</td>
          <td>115</td>
          <td>80</td>
          <td>45</td>
          <td>79</td>
          <td>136</td>
          <td>18</td>
          <td>6</td>
          <td>21</td>
          <td>5</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1505</th>
          <td>19</td>
          <td>Werder (Havel), Karl-Hagemeister-Grundsc</td>
          <td>860</td>
          <td>342</td>
          <td>3</td>
          <td>339</td>
          <td>90</td>
          <td>48</td>
          <td>42</td>
          <td>81</td>
          <td>26</td>
          <td>17</td>
          <td>1</td>
          <td>17</td>
          <td>4</td>
          <td>13</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1507</th>
          <td>19</td>
          <td>Werder (Havel), Zur alten Weberei/Angler</td>
          <td>1054</td>
          <td>559</td>
          <td>4</td>
          <td>555</td>
          <td>123</td>
          <td>102</td>
          <td>44</td>
          <td>115</td>
          <td>94</td>
          <td>15</td>
          <td>7</td>
          <td>31</td>
          <td>7</td>
          <td>14</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1513</th>
          <td>19</td>
          <td>Werder (Havel), Seniorenresidenz Haus 1</td>
          <td>395</td>
          <td>180</td>
          <td>2</td>
          <td>178</td>
          <td>57</td>
          <td>33</td>
          <td>22</td>
          <td>28</td>
          <td>13</td>
          <td>9</td>
          <td>0</td>
          <td>11</td>
          <td>1</td>
          <td>0</td>
          <td>4</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1518</th>
          <td>19</td>
          <td>Werder (Havel), GT Elisabethhöhe, GZ Bli</td>
          <td>953</td>
          <td>476</td>
          <td>5</td>
          <td>471</td>
          <td>99</td>
          <td>80</td>
          <td>46</td>
          <td>132</td>
          <td>46</td>
          <td>32</td>
          <td>3</td>
          <td>23</td>
          <td>0</td>
          <td>8</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1519</th>
          <td>19</td>
          <td>Werder (Havel), GT Plessow, Alte Schule</td>
          <td>510</td>
          <td>287</td>
          <td>2</td>
          <td>285</td>
          <td>70</td>
          <td>71</td>
          <td>21</td>
          <td>52</td>
          <td>30</td>
          <td>18</td>
          <td>0</td>
          <td>10</td>
          <td>6</td>
          <td>6</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1520</th>
          <td>19</td>
          <td>Werder (Havel), OT Plötzin, Gemeindezent</td>
          <td>281</td>
          <td>153</td>
          <td>4</td>
          <td>149</td>
          <td>29</td>
          <td>46</td>
          <td>10</td>
          <td>33</td>
          <td>11</td>
          <td>8</td>
          <td>2</td>
          <td>3</td>
          <td>2</td>
          <td>5</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1522</th>
          <td>19</td>
          <td>Werder (Havel), OT Töplitz, Haus des Bür</td>
          <td>778</td>
          <td>415</td>
          <td>5</td>
          <td>410</td>
          <td>121</td>
          <td>63</td>
          <td>31</td>
          <td>92</td>
          <td>56</td>
          <td>16</td>
          <td>1</td>
          <td>18</td>
          <td>2</td>
          <td>10</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>...</th>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
        </tr>
        <tr>
          <th>7104</th>
          <td>22</td>
          <td>Stern - In der Aue/Gluckstr.</td>
          <td>1285</td>
          <td>669</td>
          <td>5</td>
          <td>664</td>
          <td>193</td>
          <td>67</td>
          <td>134</td>
          <td>115</td>
          <td>70</td>
          <td>20</td>
          <td>5</td>
          <td>23</td>
          <td>5</td>
          <td>28</td>
          <td>4</td>
          <td>0</td>
        </tr>
        <tr>
          <th>4207</th>
          <td>22</td>
          <td>Südliche Innenstadt - Finkenweg</td>
          <td>382</td>
          <td>220</td>
          <td>0</td>
          <td>220</td>
          <td>33</td>
          <td>21</td>
          <td>47</td>
          <td>25</td>
          <td>60</td>
          <td>5</td>
          <td>6</td>
          <td>3</td>
          <td>7</td>
          <td>9</td>
          <td>4</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7106</th>
          <td>22</td>
          <td>Stern - Johannes-Kepler-Platz/Chopinstr.</td>
          <td>1101</td>
          <td>465</td>
          <td>9</td>
          <td>456</td>
          <td>144</td>
          <td>36</td>
          <td>108</td>
          <td>89</td>
          <td>31</td>
          <td>7</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>6201</th>
          <td>22</td>
          <td>Teltower Vorstadt - Kolonie Daheim</td>
          <td>1257</td>
          <td>653</td>
          <td>6</td>
          <td>647</td>
          <td>180</td>
          <td>63</td>
          <td>99</td>
          <td>64</td>
          <td>168</td>
          <td>16</td>
          <td>0</td>
          <td>28</td>
          <td>9</td>
          <td>15</td>
          <td>5</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7202</th>
          <td>22</td>
          <td>Drewitz - Sterncenter/Erich-Pommer-Str.</td>
          <td>1051</td>
          <td>436</td>
          <td>9</td>
          <td>427</td>
          <td>149</td>
          <td>45</td>
          <td>87</td>
          <td>79</td>
          <td>27</td>
          <td>12</td>
          <td>7</td>
          <td>12</td>
          <td>2</td>
          <td>7</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>6302</th>
          <td>22</td>
          <td>Schlaatz - Milanhorst</td>
          <td>812</td>
          <td>290</td>
          <td>2</td>
          <td>288</td>
          <td>72</td>
          <td>19</td>
          <td>54</td>
          <td>81</td>
          <td>26</td>
          <td>11</td>
          <td>1</td>
          <td>5</td>
          <td>5</td>
          <td>13</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7205</th>
          <td>22</td>
          <td>Drewitz - Willy-A.-Kleinau-Weg</td>
          <td>754</td>
          <td>264</td>
          <td>2</td>
          <td>262</td>
          <td>82</td>
          <td>16</td>
          <td>52</td>
          <td>67</td>
          <td>17</td>
          <td>10</td>
          <td>3</td>
          <td>8</td>
          <td>0</td>
          <td>5</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>6403</th>
          <td>22</td>
          <td>Waldstadt I - Bernhard-Kellermann-Str.</td>
          <td>1104</td>
          <td>540</td>
          <td>7</td>
          <td>533</td>
          <td>182</td>
          <td>55</td>
          <td>78</td>
          <td>110</td>
          <td>55</td>
          <td>14</td>
          <td>5</td>
          <td>15</td>
          <td>4</td>
          <td>15</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7206</th>
          <td>22</td>
          <td>Drewitz - Gerlachstr/E-v-Winterstein-Str</td>
          <td>711</td>
          <td>297</td>
          <td>2</td>
          <td>295</td>
          <td>81</td>
          <td>21</td>
          <td>45</td>
          <td>80</td>
          <td>24</td>
          <td>13</td>
          <td>3</td>
          <td>14</td>
          <td>0</td>
          <td>13</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>6503</th>
          <td>22</td>
          <td>Waldstadt II - Kiefernring/Sonnentaustr.</td>
          <td>1024</td>
          <td>401</td>
          <td>5</td>
          <td>396</td>
          <td>86</td>
          <td>41</td>
          <td>62</td>
          <td>106</td>
          <td>45</td>
          <td>17</td>
          <td>5</td>
          <td>9</td>
          <td>4</td>
          <td>20</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7301</th>
          <td>22</td>
          <td>Kirchsteigfeld - Eleonore-Prochaska-Str.</td>
          <td>1011</td>
          <td>486</td>
          <td>7</td>
          <td>479</td>
          <td>144</td>
          <td>41</td>
          <td>69</td>
          <td>92</td>
          <td>84</td>
          <td>11</td>
          <td>6</td>
          <td>14</td>
          <td>3</td>
          <td>15</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>6504</th>
          <td>22</td>
          <td>Waldstadt II - Saarmunder Str.</td>
          <td>1209</td>
          <td>407</td>
          <td>4</td>
          <td>403</td>
          <td>106</td>
          <td>31</td>
          <td>92</td>
          <td>73</td>
          <td>48</td>
          <td>15</td>
          <td>5</td>
          <td>8</td>
          <td>4</td>
          <td>17</td>
          <td>4</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7303</th>
          <td>22</td>
          <td>Kirchsteigfeld - Zum Teich/Schinkelstr.</td>
          <td>1149</td>
          <td>657</td>
          <td>2</td>
          <td>655</td>
          <td>200</td>
          <td>106</td>
          <td>105</td>
          <td>106</td>
          <td>58</td>
          <td>12</td>
          <td>7</td>
          <td>22</td>
          <td>10</td>
          <td>27</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>6505</th>
          <td>22</td>
          <td>Waldstadt II - Am Springbruch</td>
          <td>1111</td>
          <td>462</td>
          <td>6</td>
          <td>456</td>
          <td>99</td>
          <td>31</td>
          <td>85</td>
          <td>137</td>
          <td>36</td>
          <td>15</td>
          <td>3</td>
          <td>13</td>
          <td>4</td>
          <td>30</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9061</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9061</td>
          <td>0</td>
          <td>589</td>
          <td>9</td>
          <td>580</td>
          <td>142</td>
          <td>74</td>
          <td>84</td>
          <td>63</td>
          <td>150</td>
          <td>12</td>
          <td>3</td>
          <td>33</td>
          <td>4</td>
          <td>15</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>6506</th>
          <td>22</td>
          <td>Waldstadt II - Liefelds Grund</td>
          <td>914</td>
          <td>355</td>
          <td>3</td>
          <td>352</td>
          <td>102</td>
          <td>27</td>
          <td>68</td>
          <td>82</td>
          <td>28</td>
          <td>13</td>
          <td>4</td>
          <td>9</td>
          <td>6</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9062</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9062</td>
          <td>0</td>
          <td>545</td>
          <td>3</td>
          <td>542</td>
          <td>154</td>
          <td>76</td>
          <td>82</td>
          <td>73</td>
          <td>106</td>
          <td>18</td>
          <td>2</td>
          <td>16</td>
          <td>2</td>
          <td>10</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7105</th>
          <td>22</td>
          <td>Stern - Parforceheide</td>
          <td>1167</td>
          <td>549</td>
          <td>9</td>
          <td>540</td>
          <td>146</td>
          <td>54</td>
          <td>103</td>
          <td>84</td>
          <td>99</td>
          <td>7</td>
          <td>7</td>
          <td>24</td>
          <td>2</td>
          <td>13</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9063</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9063</td>
          <td>0</td>
          <td>661</td>
          <td>9</td>
          <td>652</td>
          <td>179</td>
          <td>52</td>
          <td>132</td>
          <td>134</td>
          <td>83</td>
          <td>13</td>
          <td>10</td>
          <td>16</td>
          <td>7</td>
          <td>21</td>
          <td>5</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7109</th>
          <td>22</td>
          <td>Stern - Niels-Bohr-Ring</td>
          <td>787</td>
          <td>342</td>
          <td>4</td>
          <td>338</td>
          <td>86</td>
          <td>22</td>
          <td>94</td>
          <td>88</td>
          <td>21</td>
          <td>5</td>
          <td>4</td>
          <td>4</td>
          <td>2</td>
          <td>9</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9064</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9064</td>
          <td>0</td>
          <td>1027</td>
          <td>7</td>
          <td>1020</td>
          <td>274</td>
          <td>138</td>
          <td>184</td>
          <td>144</td>
          <td>148</td>
          <td>23</td>
          <td>12</td>
          <td>50</td>
          <td>6</td>
          <td>35</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7110</th>
          <td>22</td>
          <td>Stern - Otto-Hahn-Ring</td>
          <td>895</td>
          <td>325</td>
          <td>6</td>
          <td>319</td>
          <td>90</td>
          <td>20</td>
          <td>79</td>
          <td>64</td>
          <td>27</td>
          <td>9</td>
          <td>4</td>
          <td>10</td>
          <td>1</td>
          <td>14</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9066</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9066</td>
          <td>0</td>
          <td>573</td>
          <td>0</td>
          <td>573</td>
          <td>145</td>
          <td>53</td>
          <td>136</td>
          <td>96</td>
          <td>80</td>
          <td>13</td>
          <td>3</td>
          <td>13</td>
          <td>14</td>
          <td>18</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7112</th>
          <td>22</td>
          <td>Stern - Gaußstr./Sternstr.</td>
          <td>895</td>
          <td>297</td>
          <td>1</td>
          <td>296</td>
          <td>89</td>
          <td>14</td>
          <td>61</td>
          <td>83</td>
          <td>20</td>
          <td>10</td>
          <td>2</td>
          <td>8</td>
          <td>1</td>
          <td>6</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9068</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9068</td>
          <td>0</td>
          <td>747</td>
          <td>10</td>
          <td>737</td>
          <td>209</td>
          <td>78</td>
          <td>186</td>
          <td>103</td>
          <td>86</td>
          <td>15</td>
          <td>5</td>
          <td>26</td>
          <td>1</td>
          <td>21</td>
          <td>7</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7201</th>
          <td>22</td>
          <td>Drewitz - Alt Drewitz/Am Silbergraben</td>
          <td>1045</td>
          <td>542</td>
          <td>8</td>
          <td>534</td>
          <td>156</td>
          <td>70</td>
          <td>52</td>
          <td>119</td>
          <td>69</td>
          <td>19</td>
          <td>3</td>
          <td>26</td>
          <td>5</td>
          <td>13</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>7302</th>
          <td>22</td>
          <td>Kirchsteigfeld - Priesterweg</td>
          <td>1015</td>
          <td>465</td>
          <td>9</td>
          <td>456</td>
          <td>117</td>
          <td>34</td>
          <td>79</td>
          <td>115</td>
          <td>50</td>
          <td>10</td>
          <td>4</td>
          <td>22</td>
          <td>8</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9065</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9065</td>
          <td>0</td>
          <td>624</td>
          <td>2</td>
          <td>622</td>
          <td>202</td>
          <td>57</td>
          <td>134</td>
          <td>88</td>
          <td>75</td>
          <td>17</td>
          <td>11</td>
          <td>14</td>
          <td>7</td>
          <td>17</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9070</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9070</td>
          <td>0</td>
          <td>605</td>
          <td>8</td>
          <td>597</td>
          <td>145</td>
          <td>81</td>
          <td>115</td>
          <td>106</td>
          <td>71</td>
          <td>15</td>
          <td>19</td>
          <td>25</td>
          <td>2</td>
          <td>15</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <th>9071</th>
          <td>22</td>
          <td>Potsdam Briefwahlbezirk 9071</td>
          <td>0</td>
          <td>580</td>
          <td>2</td>
          <td>578</td>
          <td>154</td>
          <td>55</td>
          <td>113</td>
          <td>74</td>
          <td>92</td>
          <td>19</td>
          <td>14</td>
          <td>27</td>
          <td>10</td>
          <td>18</td>
          <td>2</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    <p>217 rows × 18 columns</p>
    </div>



.. code:: ipython3

    df.reset_index(inplace=True)

.. code:: ipython3

    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Nr</th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>Wähler</th>
          <th>Ungült. Zweitstimmen</th>
          <th>Gültige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÜNE/B 90</th>
          <th>BVB / FREIE WÄHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÖDP</th>
          <th>Tierschutzpartei</th>
          <th>V-Partei³</th>
          <th>Sonstige</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1302</td>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1304</td>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1309</td>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1401</td>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1502</td>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
          <td>757</td>
          <td>335</td>
          <td>3</td>
          <td>332</td>
          <td>82</td>
          <td>40</td>
          <td>51</td>
          <td>68</td>
          <td>55</td>
          <td>8</td>
          <td>1</td>
          <td>14</td>
          <td>4</td>
          <td>8</td>
          <td>1</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



