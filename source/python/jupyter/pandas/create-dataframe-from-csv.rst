
CSV-Datei(en) verarbeiten
=========================

Als Beispieldaten werden die vorläufigen Wahlergebnisse der Landtagswahl
Brandenburg 2019 verwendet.

Quelle: https://opendata.potsdam.de/explore/dataset/lwerst2019/export/

.. code:: ipython3

    import pandas as pd

.. index:: Pandas; read_csv
.. index:: Pandas; head
.. index:: Pandas; shape

.. code:: ipython3

    df = pd.read_csv('./lwzweit2019.csv', delimiter=";")
    df.head()
    #df.shape




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Nr</th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
          <th>Wähler</th>
          <th>Ungült. Zweitstimmen</th>
          <th>Gültige Zweitstimmen</th>
          <th>SPD</th>
          <th>CDU</th>
          <th>DIE LINKE</th>
          <th>AfD</th>
          <th>GRÜNE/B 90</th>
          <th>BVB / FREIE WÄHLER</th>
          <th>PIRATEN</th>
          <th>FDP</th>
          <th>ÖDP</th>
          <th>Tierschutzpartei</th>
          <th>V-Partei³</th>
          <th>Sonstige</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1302</td>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1304</td>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1309</td>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1401</td>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1502</td>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
          <td>757</td>
          <td>335</td>
          <td>3</td>
          <td>332</td>
          <td>82</td>
          <td>40</td>
          <td>51</td>
          <td>68</td>
          <td>55</td>
          <td>8</td>
          <td>1</td>
          <td>14</td>
          <td>4</td>
          <td>8</td>
          <td>1</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. index:: Pandas; read_csv -- ohne header

.. code:: ipython3

    # Nochmal ohne Header falls keine Headerzeile vorhanden ist
    # dann wird durchnummeriert wie hier zu sehen.
    df = pd.read_csv('./lwzweit2019.csv', delimiter=";", header=None)
    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>0</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
          <th>8</th>
          <th>9</th>
          <th>10</th>
          <th>11</th>
          <th>12</th>
          <th>13</th>
          <th>14</th>
          <th>15</th>
          <th>16</th>
          <th>17</th>
          <th>18</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>Nr</td>
          <td>Wahlkreis</td>
          <td>Name</td>
          <td>Wahlb. insges.</td>
          <td>Wähler</td>
          <td>Ungült. Zweitstimmen</td>
          <td>Gültige Zweitstimmen</td>
          <td>SPD</td>
          <td>CDU</td>
          <td>DIE LINKE</td>
          <td>AfD</td>
          <td>GRÜNE/B 90</td>
          <td>BVB / FREIE WÄHLER</td>
          <td>PIRATEN</td>
          <td>FDP</td>
          <td>ÖDP</td>
          <td>Tierschutzpartei</td>
          <td>V-Partei³</td>
          <td>Sonstige</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1302</td>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
          <td>635</td>
          <td>4</td>
          <td>631</td>
          <td>175</td>
          <td>76</td>
          <td>86</td>
          <td>94</td>
          <td>121</td>
          <td>22</td>
          <td>2</td>
          <td>31</td>
          <td>7</td>
          <td>15</td>
          <td>2</td>
          <td>0</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1304</td>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
          <td>697</td>
          <td>4</td>
          <td>693</td>
          <td>176</td>
          <td>111</td>
          <td>59</td>
          <td>102</td>
          <td>154</td>
          <td>15</td>
          <td>2</td>
          <td>54</td>
          <td>7</td>
          <td>12</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1309</td>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
          <td>847</td>
          <td>2</td>
          <td>845</td>
          <td>196</td>
          <td>108</td>
          <td>82</td>
          <td>78</td>
          <td>239</td>
          <td>26</td>
          <td>9</td>
          <td>59</td>
          <td>18</td>
          <td>24</td>
          <td>6</td>
          <td>0</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1401</td>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
          <td>60</td>
          <td>0</td>
          <td>60</td>
          <td>16</td>
          <td>12</td>
          <td>8</td>
          <td>8</td>
          <td>11</td>
          <td>2</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>3</td>
          <td>0</td>
          <td>0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    # andere Zeile als Header
    df = pd.read_csv('./lwzweit2019.csv', delimiter="|", header=3)
    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>1309;19;Bornstedt - A.-Wolff-Platz/E.-Arendt-Str;1742;847;2;845;196;108;82;78;239;26;9;59;18;24;6;0</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1401;19;Sacrow;109;60;0;60;16;12;8;8;11;2;0;0;...</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1502;19;Eiche - Kaiser-Friedrich-Str. Süd;757;...</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1503;19;Eiche - Altes Rad West/Lindstedter Str...</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1601;19;Grube;345;210;3;207;52;31;18;37;42;13;...</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1701;19;Golm - Ehrenpfortenbergstr.;786;450;6;...</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    # Auswahl Spalten
    df = pd.read_csv('./lwzweit2019.csv', delimiter=";", usecols=['Wahlkreis', 'Name', 'Wahlb. insges.'])
    #df.style.set_properties(**{'text-align': 'left'})
    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Wahlkreis</th>
          <th>Name</th>
          <th>Wahlb. insges.</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
          <td>1183</td>
        </tr>
        <tr>
          <th>1</th>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
          <td>1259</td>
        </tr>
        <tr>
          <th>2</th>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
          <td>1742</td>
        </tr>
        <tr>
          <th>3</th>
          <td>19</td>
          <td>Sacrow</td>
          <td>109</td>
        </tr>
        <tr>
          <th>4</th>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
          <td>757</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    # leere Einträge behandeln
    df = pd.read_csv('./lwzweit2019.csv', delimiter=";",
                     usecols=['Wahlkreis', 'Name'],
                     na_values=[''])
    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Wahlkreis</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>19</td>
          <td>Bornstedt - Fliederweg</td>
        </tr>
        <tr>
          <th>1</th>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
        </tr>
        <tr>
          <th>2</th>
          <td>19</td>
          <td>Bornstedt - A.-Wolff-Platz/E.-Arendt-Str</td>
        </tr>
        <tr>
          <th>3</th>
          <td>19</td>
          <td>Sacrow</td>
        </tr>
        <tr>
          <th>4</th>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
        </tr>
      </tbody>
    </table>
    </div>



.. index:: Pandas; read_csv -- auch leere Zeilen

.. code:: ipython3

    # leere Zeilen importieren
    # leere Zeilen werden ignoriert, wenn notwendig, das Standardverhalten deaktivieren
    df = pd.read_csv('./lwzweit2019.csv', delimiter=";",
                     usecols=['Wahlkreis', 'Name'],
                     na_values=[''],
                     skip_blank_lines=False)
    df.head()
    df.shape




.. parsed-literal::

    (217, 2)



.. index:: Pandas; read_csv -- skip rows

.. code:: ipython3

    # einzelne Zeilen auslassen
    df = pd.read_csv('./lwzweit2019.csv', delimiter=";",
                     usecols=['Wahlkreis', 'Name'],
                     na_values=[''],
                     skip_blank_lines=False,
                     skiprows = [1,3,4])
    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Wahlkreis</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>19</td>
          <td>Bornstedt - Kirschallee/Thaerstr.</td>
        </tr>
        <tr>
          <th>1</th>
          <td>19</td>
          <td>Eiche - Kaiser-Friedrich-Str. Süd</td>
        </tr>
        <tr>
          <th>2</th>
          <td>19</td>
          <td>Eiche - Altes Rad West/Lindstedter Str.</td>
        </tr>
        <tr>
          <th>3</th>
          <td>19</td>
          <td>Grube</td>
        </tr>
        <tr>
          <th>4</th>
          <td>19</td>
          <td>Golm - Ehrenpfortenbergstr.</td>
        </tr>
      </tbody>
    </table>
    </div>



.. index:: Pandas; read_csv -- Zeilen am Ende weglassen

.. code:: ipython3

    # Zeilen am Ende weglassen
    df = pd.read_csv('./lwzweit2019.csv', delimiter=";",
                     usecols=['Wahlkreis', 'Name'],
                     na_values=[''],
                     skip_blank_lines=False,
                     skiprows = [1,3,4],
                     skipfooter=2, engine='python')
    df.head()
    df.shape




.. parsed-literal::

    (212, 2)



.. index:: Pandas; read_csv -- Teilmenge der Zeilen einlesen

.. code:: ipython3

    # eine begrenzte Anzahl einlesen
    df = pd.read_csv('./lwzweit2019.csv', delimiter=";",
                     usecols=['Wahlkreis', 'Name'],
                     na_values=[''],
                     skip_blank_lines=False,
                     skiprows = [1,3,4],
                     #skipfooter=2, engine='python',
                     nrows=20)
    df.head()
    df.shape




.. parsed-literal::

    (20, 2)


