======
VSCode
======


.. index:: VSCode; MacOS

Start (aus CMD heraus)
----------------------

MacOS
~~~~~

Eintrag in ~/.bash_profile

.. code-block:: bash


    code () { VSCODE_CWD="$PWD" open -n -b "com.microsoft.VSCode" --args $* ;}

Weitere Varianten sind unter folgener URL zu finden (auch für Windows):

`https://stackoverflow.com/questions/29971053/how-to-open-visual-studio-code-from-the-command-line-on-osx <https://stackoverflow.com/questions/29971053/how-to-open-visual-studio-code-from-the-command-line-on-osx>`_

Tastenkombinationen
-------------------

.. index:: VSCode

`https://code.visualstudio.com/docs/getstarted/keybindings#_keyboard-shortcuts-reference <https://code.visualstudio.com/docs/getstarted/keybindings#_keyboard-shortcuts-reference>`_


Windows-Benutzer verwenden die Ctrl-Tast, Mac-User die  Cmd-Taste!

.. table::

    +-------------+--------------+------------------------+--------------------------+
    | MacOS/Linux | Windows      | Objekt                 | Action                   |
    +=============+==============+========================+==========================+
    | Alt         | \            | Datei                  | Datei in neuem Fenster   |
    +-------------+--------------+------------------------+--------------------------+
    | Cmd+P       | Ctrl+P       | Datei                  | schnelles Öffnen         |
    +-------------+--------------+------------------------+--------------------------+
    | Shift+Cmd+P | Shift+Ctrl+P | Konfiguration          | `pythonPath definieren`_ |
    +-------------+--------------+------------------------+--------------------------+
    | Shift+Cmd+B | Shift+Ctrl+B | Termina/Run Build Task | Build-Prozess starten    |
    +-------------+--------------+------------------------+--------------------------+

Konfiguration
-------------

.. index:: VSCode; Kofiguration

- `https://code.visualstudio.com/docs/python/settings-reference <https://code.visualstudio.com/docs/python/settings-reference>`_

pythonPath definieren
---------------------

.. index:: VSCode; PythonPath

VSCode bietet die Standardinterpreter des Systems an. Über die Konfiguration:

Shift+Cmd+P/Shift+Cmd+P kann nach pythonPath gesucht und dort auch
der Pfad angepasst werden.

.. image:: ./images/vscode-pythonpath.png

In *settings.json* steht dann soetwas drin:

.. code-block:: js


    {
      "python.pythonPath": "~/labs/bfinder/bin/python"
    }

Pyramid-Start-Konfiguration
---------------------------

.. index:: VSCode; Pyramid

.. code-block:: js


    {
     // Use IntelliSense to learn about possible attributes.
     // Hover to view descriptions of existing attributes.
     // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
     "version": "0.2.0",
     "configurations": [

         {
    	 "name": "Python: Pyramid-Anwendung",
    	 "type": "python",
    	 "request": "launch",
    	 "module": "pyramid.scripts.pserve",
    	 "args": [
    	     "${workspaceFolder}/development.ini"
    	 ],
    	 "pyramid": true,
    	 "jinja": true
         }
     ]
    }

Sphinx: make html
-----------------

.. code-block:: js


       {
        "version": "2.0.0",
        "tasks": [
          {
    	"taskName": "make html",
    	"command": "${workspaceFolder}\\ozg-zentral\\make.bat",
    	"args": [
    	  "html"
    	],
    	"isShellCommand": true,
    	"group": {
    	  "kind": "build",
    	  "isDefault": true
    	}
          }
        ]
    }

Download und Installation
-------------------------

Mit Microsoft-Telemetrie
------------------------

`https://code.visualstudio.com/Download <https://code.visualstudio.com/Download>`_
`https://code.visualstudio.com/docs <https://code.visualstudio.com/docs>`_

Ohne Microsoft-Telemetrie
-------------------------

`https://vscodium.com/ <https://vscodium.com/>`_

Debian
~~~~~~

als sudo:

.. code-block:: bash


    wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \ 
    | sudo apt-key add -
    echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ \ 
    vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list 

    apt update && sudo apt install codium  

Datenbankanbindung
------------------

.. index:: VS-Code; Datenbank (MS-SQL-Server)

Für Windows gibt es die Möglichkeit MS-SQL-Server einzubinden und
VS-Code als SQL-Client einzusetzen.

.. image:: ./images/vscode-datenbank-ms-sql.png
    :width: 550px
