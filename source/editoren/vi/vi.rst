.. index:: vi
============
VI-Kommandos
============

=========== ==========================
Kommando    Anmerkung
----------- --------------------------
i           Einfügemodus
A           Einfügen am Ende der Zeile 
Esc,:wq     Speichern und Beenden
Esc,:q!     Beenden ohne Speichern
=========== ==========================

  