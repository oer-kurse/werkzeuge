.. Toolbox documentation master file, created by
   sphinx-quickstart on Sun Sep 20 14:08:09 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
Start
=====

| Immer wieder muss was installiert und ausprobiert werden.
| Nachfolgendeeine Liste mit Hinweisen und Protokollen...

Editoren
========

.. toctree::


   editoren/index


Entwicker-Werkzeuge
===================

.. toctree::
   :maxdepth: 2

   develop/git/index
   develop/mercurial/mercurial


Dokumentation
=============

.. toctree::
   :maxdepth: 2

   python/sphinx-by-example/index


Datenbanken
===========

.. toctree::
   :maxdepth: 2

   datenbanken/index

   
Projektmanagement
=================
.. toctree::
   :maxdepth: 1

   develop/uml/tools
   develop/cookiecutter/cookiecutter


Python
======

.. toctree::
   :maxdepth: 2
   :glob:
      
   python/index

   
XML
===

.. toctree::
   :maxdepth: 1

   xml/index


Administration
==============

.. toctree::
   :glob:
   :maxdepth: 1

   develop/ansible/ansible
   develop/docker/index
   develop/admin/check_mk/index
   develop/admin/check_mk/plugin
   
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
