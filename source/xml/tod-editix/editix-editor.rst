
:ref:`« Übersicht Werkzeuge <xml-tools-start>`
     
.. index:: Werkzeuge; Editix
.. index:: Tools; Editix

============================
Editix (Windows/MacOS/Linux)
============================

.. image:: auto.jpg
   :width: 0px
	   
.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Brücke
         <span>
           <img src='../../_images/auto.jpg'
                alt='Auto' />
          </span>
         </a>
        </div>

.. sidebar:: SQL-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Dinge

  
Im `XML-Editix-Professional (30 Tage-Kostenlos-Test)`_: können Sie
alle Arbeiten an XML-Dokumenten und vieles mehr erledigen.

Wer knapp bei Kasse ist und mit Einschränkungen leben kann, versucht
es mit der `XML-Editix-Community`_ -Version...

.. image:: editix.png 


Installation: Editx (community-Version)
=======================================

1. Java prüfen

   ::

      java -version
  
      1.8.xxx

2. Ant installieren

   ::
  
         https://ant.apache.org/bindownload.cgi
        
          apache-ant-1.10.7-bin.zip
2.1. Systemeinstellungen in Windows

   Alt+Pause; Erweiterte Systemeinstellungen, Umgebungsvariablen
    
   :: 

     Umgebungsvariable: ANT_HOME="c:\home..."
     Path erweitern:    %ANT_HOME%\bin
  
  
3. Editx

   - https://www.editix.com/download.html
   - https://github.com/AlexandreBrillant/Editix-xml-editor


3.1 Entpacken und umbenennen: Editix
3.2 Start

    An der Kommandozeile im Editix-Ordner:
    
    ::
  
      run.bat <-- Windows
      run.sh  <-- Linux/MacOS

	   
.. _XML-Editix-Professional (30 Tage-Kostenlos-Test): https://editix.com/download.html
.. _XML-Editix-Community: https://github.com/AlexandreBrillant/Editix-xml-editor
