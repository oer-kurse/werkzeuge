<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template match="adresse">
    <table border="1"><xsl:apply-templates select="person"/></table>
  </xsl:template>
    <xsl:template match="person">
    <tr><xsl:apply-templates select="name"/></tr>
  </xsl:template>
  
  <xsl:template match="nachname">
    <td><xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="vorname">
    <td><xsl:value-of select="."/></td>
  </xsl:template>
</xsl:stylesheet>
   
