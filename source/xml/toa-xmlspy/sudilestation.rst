.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML-Spy, OER

.. _xmlspy:

:ref:`« Übersicht Werkzeuge <xml-tools-start>`
     
.. index:: Werkzeuge; XML-Spy
.. index:: Tools; XML-Spy

=================
XML-Spy (Windows)
=================

.. image:: ../werbung/bruecke-postdam-westkreuz.jpg
   :width: 0px
	   
.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Brücke
         <span>
           <img src='../../_images/bruecke-postdam-westkreuz.jpg'
                alt='Brücke' />
          </span>
         </a>
        </div>

.. sidebar:: SQL-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Brücken

  
Im `XML-Spy (30 Tage-Test)`_: können Sie ihre Dokumente prüfen lassen. 
Den Sytax-Check bewerkstelligen Sie, indem Sie auf das folgende
Bild im XML-Spy klicken.

.. image:: ../img/pruefung.png
   
Sie können dabei auf die *Gültigkeit* als auch die  *Wohlgeformtheit* Ihrer
aktuellen XML-Dokumente prüfen.

Für das Erlernen von XML, XSL und vielen anderen Technkiken kann
diese hervorragende Software wärmstens empfohlen werden. Linux- und
MacOS-Benutzer haben die Möglichkeit mit libxml oder Saxon zu
prüfen. 
 

.. _XML-Spy (30 Tage-Test): https://www.altova.com/xmlspy-xml-editor/download 
