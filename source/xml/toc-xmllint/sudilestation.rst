.. _xmllint:

.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Werkzeuge, Tools, Saxon, Java, OER, xmllint

:ref:`« Übersicht Werkzeuge <xml-tools-start>`
     
.. index:: Werkzeuge; xmllint
.. index:: Tools; xmllint

=======
xmllint
=======

.. image:: ../werbung/physiotherapie.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Physiotherapie
         <span>
           <img src='../../_images/physiotherapie.jpg'
                alt='Physiotherapie' />
          </span>
         </a>
        </div>

.. sidebar:: SQL-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Spaß


Lernziel
========

Wenn eine XML-Datei nach einem Regelwerk (XSD) aufgebaut sein soll, läßt sich
die XML-Datei mit dem Programm xmllint überprüfen.



Handlungsanweisungen
====================

:Aufgaben:

   1. Prüfen Sie, ob xmllint verfügbar ist (Teil der libxml-Bibliothek)!
   2. Überprüfen Sie eine XML-Datei, die zu einer XSD kompatibel sein soll.
 

Beispiel für den Aufruf
=======================
Shell öffnen...

.. code-block:: bash

  cd  mein-ordner-mit-dem-xml-kram
  xmllint --schema meine.xsd meine.xml
  


